<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <title>${requestScope.blog != null ? "Update Product" : "Create Product"}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Book-shop</title>
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
</head>
<%@include file="../../../layout/header.jsp" %>
<body>
    <%@include file="../../../layout/LeftAdminSaleBar.jsp" %>
    <section style="margin-left: 280px; height: calc(92vh); overflow-y: auto;">
        <div class="card my-2 mx-2">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="fs-2 fw-bold">
                        ${requestScope.blog != null ? "UPDATE BLOG" : "CREATE BLOG"}
                    </h1>
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="manage-blog" id="submitForm" enctype="multipart/form-data">
                    <div class="col">
                        <div class="row">
                            <input type="hidden" name="action"
                                   value="${requestScope.blog != null ? "U" : "C"}">
                            <c:if test="${requestScope.blog != null}">
                                <input type="hidden" name="blogId"
                                       value="${requestScope.blog.blogId}">
                            </c:if>
                            <div class="col-6">
                                <label for="category-film" class="col-form-label fw-bold">Title:</label>
                                <input type="text" class="form-control"
                                       name="title" value="${requestScope.blog.title}" required minlength="20">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label for="category-film" class="col-form-label fw-bold">Blog Content: </label>
                                <textarea class="form-control" rows="8" name="content" minlength="150" required>${requestScope.blog.blogContent}</textarea>
                            </div>
                        </div>
                        <c:if test="${requestScope.blog.blogImage == null}">
                            <div class="mt-3">
                                <label for="exampleFormControlFile1" class="col-form-label fw-bold">Image: </label>
                                <input type="file" class="form-control-file mt-2" name="file" id="file" accept="image/*" required>
                                <span id="file" class="mt-2" style="color: red; display: none;"></span>
                            </div>  
                        </c:if>
                        <c:if test="${requestScope.blog.blogImage != null}">
                            <div class="mt-3">
                                <label for="exampleFormControlFile1" class="col-form-label">Update Image</label>
                                <input type="file" class="form-control-file mt-2" name="file" id="file" accept="image/*">
                                <input type="hidden" name="image" value="${requestScope.blog.blogImage}">
                                <img style="width: 50px; height: 50px; object-fit: cover; border-radius: 8px;"
                                     src="${requestScope.blog.blogImage}"
                                     alt="book-shop">
                            </div>  
                        </c:if>
                    </div>
                    <div class="row">
                        <div class="d-flex justify-content-center gap-3 col-6">
                            <c:if test="${requestScope.blog != null}">
                                <a type="button" class="btn btn-secondary btn-lg" href="sale-admin-list-blog?blogId=${requestScope.blog.blogId}">
                                    Cancel</a>
                                </c:if>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>

<!-- Script using  -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/8d39de38b8.js" crossorigin="anonymous"></script>
<script>
</script>
