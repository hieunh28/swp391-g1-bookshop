/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Book;
import Model.Constant;
import Model.RateBook;
import Model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bau
 */
public class RateBookDAO extends DBContext {

    /**
     *
     * @param rate
     */
    public void insertRate(RateBook rate) {
        try {
            String sql = "INSERT INTO [dbo].[RateBooks]\n"
                    + "           ([Rate]\n"
                    + "           ,[Comment]\n"
                    + "           ,[CustomerId]\n"
                    + "           ,[BookId]\n"
                    + "           ,[Date]\n"
                    + "           ,[Status]\n"
                    + "           ,[DeleteFlag])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setFloat(1, rate.getRate());
            stm.setString(2, rate.getComment());
            stm.setInt(3, rate.getCustomerId());
            stm.setInt(4, rate.getBookId());
            stm.setDate(5, rate.getDate());
            stm.setBoolean(6, rate.getStatus());
            stm.setBoolean(7, rate.getDeleteFlag());

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RateBookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param offset
     * @param RecordPerPage
     * @return
     */
    public ArrayList<RateBook> getRatePagnition(int offset, int RecordPerPage) {
        ArrayList<RateBook> list = new ArrayList<>();
        int count = 0;
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[RateBooks] Where DeleteFlag = 0";
            HashMap<Integer, Object> setter = new HashMap<>();

            sql += "    Order by Date desc\n";
            sql += "    Offset ? rows\n"
                    + "  Fetch next ? row only";
            setter.put(++count, offset);
            setter.put(++count, RecordPerPage);

            PreparedStatement stm = connection.prepareStatement(sql);
            for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                stm.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                UserDAO uDao = new UserDAO();
                User customer = uDao.getUserByID(rs.getInt("CustomerId"));

                BookDAO bDao = new BookDAO();
                Book book = bDao.getBookById(rs.getInt("BookId"));

                RateBook rate = new RateBook();
                rate.setRateID(rs.getInt("RateID"));
                rate.setRate(rs.getFloat("Rate"));
                rate.setComment(rs.getString("Comment"));
                rate.setUser(customer);
                rate.setBook(book);
                rate.setDate(rs.getDate("Date"));
                rate.setStatus(rs.getBoolean("Status"));

                list.add(rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RateBookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        RateBookDAO rbDao = new RateBookDAO();
        System.out.println(rbDao.getTotalRate());
    }

    /**
     *
     * @return
     */
    public int getTotalRate() {
        try {
            String sql = "SELECT count(*)\n"
                    + "  FROM [dbo].[RateBooks] Where DeleteFlag = 0";
            HashMap<Integer, Object> setter = new HashMap<>();

            PreparedStatement stm = connection.prepareStatement(sql);
            for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                stm.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RateBookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    /**
     *
     * @param rateId
     * @param StatusActive
     */
    public void updateStatusRate(int rateId, boolean StatusActive) {
        try {
            String sql = "UPDATE [dbo].[RateBooks]\n"
                    + "   SET [Status] = ?\n"
                    + " WHERE RateID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setBoolean(1, StatusActive);
            stm.setInt(2, rateId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RateBookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param rateId
     */
    public void deleteRate(int rateId) {
        try {
            String sql = "UPDATE [dbo].[RateBooks]\n"
                    + "   SET [DeleteFlag] = ?\n"
                    + " WHERE RateID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setBoolean(1, Constant.DeleteTrue);
            stm.setInt(2, rateId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RateBookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param bookId
     * @return
     */
    public ArrayList<RateBook> getAllRateByBook(int bookId) {
        ArrayList<RateBook> list = new ArrayList<>();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[RateBooks] Where DeleteFlag = 0 and BookId = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, bookId);
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                UserDAO uDao = new UserDAO();
                User customer = uDao.getUserByID(rs.getInt("CustomerId"));

                BookDAO bDao = new BookDAO();
                Book book = bDao.getBookById(rs.getInt("BookId"));

                RateBook rate = new RateBook();
                rate.setRateID(rs.getInt("RateID"));
                rate.setRate(rs.getFloat("Rate"));
                rate.setComment(rs.getString("Comment"));
                rate.setUser(customer);
                rate.setBook(book);
                rate.setDate(rs.getDate("Date"));
                rate.setStatus(rs.getBoolean("Status"));

                list.add(rate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RateBookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
