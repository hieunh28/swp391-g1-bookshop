/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Bau
 */
public class BookSortBinding {

    /**
     *
     */
    public static final int BookIdAsc = 1;

    /**
     *
     */
    public static final int BookIdDesc = 2;

    /**
     *
     */
    public static final int TitleAsc = 3;

    /**
     *
     */
    public static final int TitleDesc = 4;

    /**
     *
     */
    public static final int PriceAsc = 5;

    /**
     *
     */
    public static final int PriceDesc = 6;
}
