<section class="position-absolute d-flex flex-column flex-shrink-0 p-3 bg-light border-end"           
         style="width: 280px; height: calc(92vh);">
    <ul class="nav nav-pills flex-column mb-auto">        
        <li>
            <a href="${pageContext.request.contextPath}/admin-sale-orders" class="nav-link
               ${pageContext.request.servletPath.equals("/views/SaleAdmin/Order/list.jsp")? "active" : ""}">
                Manage Order
            </a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/admin-sale-delivery-orders" class="nav-link
               ${pageContext.request.servletPath.equals("/views/SaleAdmin/OrderDelivery/list.jsp")? "active" : ""}">
                Manage Delivery Order
            </a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/admin-customer" class="nav-link
               ${pageContext.request.servletPath.equals("/views/SaleAdmin/Customer/list.jsp")? "active" : ""}">
                Manage Customer
            </a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/sale-admin-list-blog" class="nav-link
               ${pageContext.request.servletPath.equals("/views/SaleAdmin/Blog/list.jsp")? "active" : ""}">
                Manage Blog
            </a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/list-rate-order" class="nav-link
               ${pageContext.request.servletPath.equals("/views/SaleAdmin/RateOrder/list.jsp")? "active" : ""}">
                Manage Rating
            </a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/list-refund" class="nav-link
               ${pageContext.request.servletPath.equals("/views/SaleAdmin/Refund/list.jsp")? "active" : ""}">
                Manage Refund
            </a>
        </li>
    </ul>
    <footer class="position-absolute bottom-0 start-0 end-0 text-center p-3" style="background-color: #deded540">
        @ 2023 Copyright by Book Shop
    </footer>
</section>
