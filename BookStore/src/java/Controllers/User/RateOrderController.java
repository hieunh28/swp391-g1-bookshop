/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.User;

import DAL.OrderDetailsDAO;
import DAL.RateBookDAO;
import Model.Constant;
import Model.RateBook;
import Model.User;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;

/**
 *
 * @author Bau
 */
public class RateOrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = request.getHeader("Referer");
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        if (account == null) {
            session.setAttribute("msg", "You need to login to rate book!");
            response.sendRedirect("home");
        } else {
            int userId = account.getUserID();
            Date date = Date.valueOf(LocalDate.now());
            int rateValue;

            int bookId = Integer.parseInt(request.getParameter("bookId"));
            if (request.getParameter("rate") != null) {
                rateValue = Integer.parseInt(request.getParameter("rate"));
            } else {
                rateValue = 0;
            }
            String comment = request.getParameter("comment");

            RateBook rate = new RateBook();
            rate.setBookId(bookId);
            rate.setCustomerId(userId);
            rate.setComment(comment);
            rate.setRate(rateValue);
            rate.setDate(date);
            rate.setStatus(Constant.StatusDeactive);
            rate.setDeleteFlag(Constant.DeleteFalse);

            RateBookDAO rbDao = new RateBookDAO();
            rbDao.insertRate(rate);

            OrderDetailsDAO odDao = new OrderDetailsDAO();
            int odId = Integer.parseInt(request.getParameter("orderDetailId"));
            odDao.changeStatusRate(odId, Constant.StatusActive);

            session.setAttribute("msg", "Rate book successful!");
            response.sendRedirect(url);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
