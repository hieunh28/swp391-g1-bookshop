/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Bau
 */
public class Role {

    private int id;
    private String name;
    private boolean status;
    private boolean deleteFlag;

    /**
     *
     * @param id
     * @param name
     * @param status
     * @param deleteFlag
     */
    public Role(int id, String name, boolean status, boolean deleteFlag) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @param id
     * @param name
     * @param deleteFlag
     */
    public Role(int id, String name, boolean deleteFlag) {
        this.id = id;
        this.name = name;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    public boolean isStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     *
     * Constructor
     */
    public Role() {
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public boolean isDeleteFlag() {
        return deleteFlag;
    }

    /**
     *
     * @param deleteFlag
     */
    public void setDeleteFlag(boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Role{" + "id=" + id + ", name=" + name + ", status=" + status + ", deleteFlag=" + deleteFlag + '}';
    }

}
