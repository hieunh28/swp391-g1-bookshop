/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author dell
 */
public class Refund {
    private int refundId;
    private Book book;
    private User user;
    private String reason;
    private String pickUpAddress;
    private int status;
    private Date dateRefund;

    public Refund() {
    }

    public Refund(int refundId, Book bookId, User userId, String reason, String pickUpAddress, int status) {
        this.refundId = refundId;
        this.book = bookId;
        this.user = userId;
        this.reason = reason;
        this.pickUpAddress = pickUpAddress;
        this.status = status;
    }

    public Refund(int refundId, Book bookId, User userId, String reason, String pickUpAddress, int status, Date dateRefund) {
        this.refundId = refundId;
        this.book = bookId;
        this.user = userId;
        this.reason = reason;
        this.pickUpAddress = pickUpAddress;
        this.status = status;
        this.dateRefund = dateRefund;
    }

    public Date getDateRefund() {
        return dateRefund;
    }

    public void setDateRefund(Date dateRefund) {
        this.dateRefund = dateRefund;
    }
    
    

    public int getRefundId() {
        return refundId;
    }

    public void setRefundId(int refundId) {
        this.refundId = refundId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
}
