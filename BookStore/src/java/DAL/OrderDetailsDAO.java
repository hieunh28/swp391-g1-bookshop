/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.OrderDetail;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bau
 */
public class OrderDetailsDAO extends DBContext {

    /**
     *
     * @param od
     */
    public void inset(OrderDetail od) {
        try {
            String sql = "INSERT INTO [dbo].[OrderDetails]\n"
                    + "           ([OrderId]\n"
                    + "           ,[BookId]\n"
                    + "           ,[Quantity]\n"
                    + "           ,[IsRated])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, od.getOrderId());
            stm.setInt(2, od.getBookId());
            stm.setInt(3, od.getQuantity());
            stm.setBoolean(4, od.getIsRated());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     * @param odId
     * @param isRated
     */
    public void changeStatusRate(int odId, boolean isRated) {
        try {
            String sql = "UPDATE [dbo].[OrderDetails]\n"
                    + "   SET [IsRated] = ?\n"
                    + " WHERE OrderDetailId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setBoolean(1, isRated);
            stm.setInt(2, odId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        public void changeStatusRefund(int odId, boolean isRefund) {
        try {
            String sql = "UPDATE [dbo].[OrderDetails]\n"
                    + "   SET [IsRefund] = ?\n"
                    + " WHERE OrderDetailId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setBoolean(1, isRefund);
            stm.setInt(2, odId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
