/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author Bau
 */
public class Author {
    private int authorId;
    private String authorName;
    private Date dob;
    private Boolean status;
    private Boolean deleteFlag;
    private Book[] books;

    /**
     *
     * Constructor
     */
    public Author() {
    }

    /**
     *
     * @param authorId
     * @param authorName
     * @param dob
     * @param status
     * @param deleteFlag
     * @param books
     */
    public Author(int authorId, String authorName, Date dob, Boolean status, Boolean deleteFlag, Book[] books) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.dob = dob;
        this.status = status;
        this.deleteFlag = deleteFlag;
        this.books = books;
    }

    /**
     *
     * @param authorName
     * @param dob
     * @param status
     * @param deleteFlag
     * @param books
     */
    public Author(String authorName, Date dob, Boolean status, Boolean deleteFlag, Book[] books) {
        this.authorName = authorName;
        this.dob = dob;
        this.status = status;
        this.deleteFlag = deleteFlag;
        this.books = books;
    }

    /**
     *
     * @return
     */
    public int getAuthorId() {
        return authorId;
    }

    /**
     *
     * @param authorId
     */
    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    /**
     *
     * @return
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     *
     * @param authorName
     */
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    /**
     *
     * @return
     */
    public Date getDob() {
        return dob;
    }

    /**
     *
     * @param dob
     */
    public void setDob(Date dob) {
        this.dob = dob;
    }

    /**
     *
     * @return
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    /**
     *
     * @param deleteFlag
     */
    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    public Book[] getBooks() {
        return books;
    }

    /**
     *
     * @param books
     */
    public void setBooks(Book[] books) {
        this.books = books;
    }

    /**
     *
     * @param authorId
     * @param authorName
     * @param dob
     * @param status
     * @param deleteFlag
     */
    public Author(int authorId, String authorName, Date dob, Boolean status, Boolean deleteFlag) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.dob = dob;
        this.status = status;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Author{" + "authorId=" + authorId + ", authorName=" + authorName + ", dob=" + dob + ", status=" + status + ", deleteFlag=" + deleteFlag + '}';
    }
    
}
