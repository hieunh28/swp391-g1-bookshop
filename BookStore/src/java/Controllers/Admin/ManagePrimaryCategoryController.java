/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controllers.Admin;

import Controllers.Authenticate.BaseAuthenticationController;
import DAL.CategoryDAO;
import DAL.PrimaryCategoryDAO;
import Model.Category;
import Model.Constant;
import Model.PrimaryCategory;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


public class ManagePrimaryCategoryController extends BaseAuthenticationController {
   
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    protected void processAdminGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrimaryCategoryDAO pcDao = new PrimaryCategoryDAO();
        if (request.getParameter("categoryId") != null) {
            PrimaryCategory pcategory = pcDao.getPCategoryById(Integer.parseInt(request.getParameter("categoryId")));
            request.setAttribute("category", pcategory);
        }
        request.getRequestDispatcher("views/Admin/PrimaryCategory/create.jsp").forward(request, response);
    }

    @Override
    protected void processAdminPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        PrimaryCategoryDAO pcDao = new PrimaryCategoryDAO();
        PrimaryCategory pcategory = new PrimaryCategory();
        int categoryId;
        String name;
        switch (action) {
            case Constant.Create:
                //get value is send from client
                name = request.getParameter("name");

                pcategory.setCategoryName(name);
                pcategory.setStatus(Constant.StatusActive);
                pcategory.setDeleteFlag(Constant.DeleteFalse);

                pcDao.insertPCategory(pcategory);
                break;
            case Constant.Update:
                //get value is send from client
                categoryId = Integer.parseInt(request.getParameter("categoryId"));
                name = request.getParameter("name");

                pcategory.setCategoryId(categoryId);
                pcategory.setCategoryName(name);

                pcDao.updateCategory(pcategory);
                break;
        }
        response.sendRedirect("list-primary-category");
    }

    @Override
    protected void processSaleAdminGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    protected void processSaleAdminPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
