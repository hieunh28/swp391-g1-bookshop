/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controllers;

import Model.User;
import java.sql.Date;
import java.util.ArrayList;


public class Blog {
    private int blogId;
    private User user;
    private String title;
    private Date datePost;
    private String blogContent;
    private String blogImage;
    private boolean status;
    private boolean deleteFlag;

    public Blog() {
    }

    public Blog(int blogId, User user, String title, Date datePost, String blogContent, String blogImage, boolean status, boolean deleteFlag) {
        this.blogId = blogId;
        this.user = user;
        this.title = title;
        this.datePost = datePost;
        this.blogContent = blogContent;
        this.blogImage = blogImage;
        this.status = status;
        this.deleteFlag = deleteFlag;
    }
  
    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDatePost() {
        return datePost;
    }

    public void setDatePost(Date datePost) {
        this.datePost = datePost;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    public String getBlogImage() {
        return blogImage;
    }

    public void setBlogImage(String blogImage) {
        this.blogImage = blogImage;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    
    
}
