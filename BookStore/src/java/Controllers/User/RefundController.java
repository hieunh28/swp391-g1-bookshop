/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controllers.User;

import DAL.OrderDetailsDAO;
import DAL.RefundDAO;
import Model.Book;
import Model.Constant;
import Model.Refund;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;

/**
 *
 * @author dell
 */
public class RefundController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String url = request.getHeader("Referer");
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        if (account == null) {
            session.setAttribute("msg", "You need to login to refund book!");
            response.sendRedirect("home");
        } else {
            //get current date
            Date date = Date.valueOf(LocalDate.now());

            //get book refund
            int bookId = Integer.parseInt(request.getParameter("bookId"));
            Book book = new Book();
            book.setBookId(bookId);
            
            //get reason
            String reason = request.getParameter("reason");
            String address = request.getParameter("address");
            
            Refund rf = new Refund();
            rf.setBook(book);
            rf.setUser(account);
            rf.setDateRefund(date);
            rf.setPickUpAddress(address);
            rf.setReason(reason);
            rf.setStatus(Constant.OrderPending);

            RefundDAO rDao = new RefundDAO();
            rDao.insertRefund(rf);

            OrderDetailsDAO odDao = new OrderDetailsDAO();
            int odId = Integer.parseInt(request.getParameter("orderDetailId"));
            odDao.changeStatusRefund(odId, Constant.StatusActive);

            session.setAttribute("msg", "Refund book successful!");
            response.sendRedirect(url);
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
