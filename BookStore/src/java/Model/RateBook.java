/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author Bau
 */
public class RateBook {
    private int rateID;
    private float rate;
    private String comment;
    private int customerId;
    private int bookId;
    private Date date;
    private Boolean deleteFlag;
    private Boolean status;
    
    private Book book;
    private User user;

    /**
     *
     * Constructor
     */
    public RateBook() {
    }

    /**
     *
     * @param rateID
     * @param rate
     * @param comment
     * @param customerId
     * @param bookId
     * @param date
     * @param deleteFlag
     * @param status
     * @param book
     * @param user
     */
    public RateBook(int rateID, float rate, String comment, int customerId, int bookId, Date date, Boolean deleteFlag, Boolean status, Book book, User user) {
        this.rateID = rateID;
        this.rate = rate;
        this.comment = comment;
        this.customerId = customerId;
        this.bookId = bookId;
        this.date = date;
        this.deleteFlag = deleteFlag;
        this.status = status;
        this.book = book;
        this.user = user;
    }

    /**
     *
     * @return
     */
    public int getRateID() {
        return rateID;
    }

    /**
     *
     * @param rateID
     */
    public void setRateID(int rateID) {
        this.rateID = rateID;
    }

    /**
     *
     * @return
     */
    public float getRate() {
        return rate;
    }

    /**
     *
     * @param rate
     */
    public void setRate(float rate) {
        this.rate = rate;
    }

    /**
     *
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     *
     * @param customerId
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     *
     * @return
     */
    public int getBookId() {
        return bookId;
    }

    /**
     *
     * @param bookId
     */
    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    /**
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    /**
     *
     * @param deleteFlag
     */
    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public Book getBook() {
        return book;
    }

    /**
     *
     * @param book
     */
    public void setBook(Book book) {
        this.book = book;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "RateBook{" + "rateID=" + rateID + ", rate=" + rate + ", comment=" + comment + ", customerId=" + customerId + ", bookId=" + bookId + ", date=" + date + ", deleteFlag=" + deleteFlag + ", status=" + status + ", book=" + book + ", user=" + user + '}';
    }
        
}
