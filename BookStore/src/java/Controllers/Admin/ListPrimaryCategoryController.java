/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controllers.Admin;

import Controllers.Authenticate.BaseAuthenticationController;
import DAL.CategoryDAO;
import DAL.PrimaryCategoryDAO;
import Model.Category;
import Model.Constant;
import Model.PrimaryCategory;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;


public class ListPrimaryCategoryController extends BaseAuthenticationController {
   
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    protected void processAdminGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrimaryCategoryDAO pcDao = new PrimaryCategoryDAO();

        if (request.getParameter("categoryId") != null && !request.getParameter("categoryId").isEmpty()) {
            PrimaryCategory pcategory = pcDao.getPCategoryById(Integer.parseInt(request.getParameter("categoryId")));
            request.setAttribute("category", pcategory);
            request.getRequestDispatcher("/views/Admin/PrimaryCategory/details.jsp").forward(request, response);
        } else {
            int page = 1;
            if (request.getParameter("page") != null) {
                page = Integer.parseInt(request.getParameter("page"));
            }

            ArrayList<PrimaryCategory> categories = pcDao.getAllPCategoriesPagnition((page - 1) * Constant.RecordPerPage, Constant.RecordPerPage);
            int totalCategories = pcDao.getTotalPCategories();
            int totalPage = (int) Math.ceil((double) totalCategories / Constant.RecordPerPage);
            
            request.setAttribute("items", categories);

            request.setAttribute("totalPage", totalPage);
            request.setAttribute("currentPage", page);

            request.getRequestDispatcher("/views/Admin/PrimaryCategory/list.jsp").forward(request, response);
        }
    }

    @Override
    protected void processAdminPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    protected void processSaleAdminGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    protected void processSaleAdminPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }


}
