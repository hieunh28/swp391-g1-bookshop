/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Bau
 */
public class EncodeMD5 {

    /**
     *
     * @param input
     * @return
     */
    public String EncoderMD5(String input){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] inputBytes = input.getBytes();
            
            byte[] hashBytes = md.digest(inputBytes);
            
            
            StringBuilder sb = new StringBuilder();
            for (byte b : hashBytes) {
                sb.append(String.format("%02x", b));
            }
            String md5Hash = sb.toString();
            return md5Hash;
        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }
    
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        EncodeMD5 encode = new EncodeMD5();
        System.out.println(encode.EncoderMD5("123@123"));
    }
}
