/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Bau
 */
public class Cart {
    private int cartId;
    private User customer;
    private Book book;
    private int quantity;

    /**
     *
     * Constructor
     */
    public Cart() {
    }

    /**
     *
     * @param cartId
     * @param customer
     * @param book
     * @param quantity
     */
    public Cart(int cartId, User customer, Book book, int quantity) {
        this.cartId = cartId;
        this.customer = customer;
        this.book = book;
        this.quantity = quantity;
    }

    /**
     *
     * @return
     */
    public int getCartId() {
        return cartId;
    }

    /**
     *
     * @param cartId
     */
    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    /**
     *
     * @return
     */
    public User getCustomer() {
        return customer;
    }

    /**
     *
     * @param customer
     */
    public void setCustomer(User customer) {
        this.customer = customer;
    }

    /**
     *
     * @return
     */
    public Book getBook() {
        return book;
    }

    /**
     *
     * @param book
     */
    public void setBook(Book book) {
        this.book = book;
    }

    /**
     *
     * @return
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
}
