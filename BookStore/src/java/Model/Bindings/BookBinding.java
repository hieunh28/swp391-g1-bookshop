/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model.Bindings;

/**
 *
 * @author Bau
 */
public class BookBinding {
    int authorId;
    int categoryID;
    int publisherID;
    String textSearch = "";
    int status;
    double minPrice; 
    double maxPrice = Double.MAX_VALUE;
    
    /**
     *
     * @return
     */
    public int getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     *
     * @param authorId
     */
    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    /**
     *
     * @param categoryID
     */
    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    /**
     *
     * @param textSearch
     */
    public void setTextSearch(String textSearch) {
        this.textSearch = textSearch;
    }

    /**
     *
     * @param minPrice
     */
    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    /**
     *
     * @param maxPrice
     */
    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    /**
     *
     * Constructor
     */
    public BookBinding() {
    }

    /**
     *
     * @return
     */
    public int getAuthorId() {
        return authorId;
    }

    /**
     *
     * @return
     */
    public int getCategoryID() {
        return categoryID;
    }

    /**
     *
     * @return
     */
    public String getTextSearch() {
        return textSearch;
    }

    /**
     *
     * @return
     */
    public double getMinPrice() {
        return minPrice;
    }

    /**
     *
     * @return
     */
    public double getMaxPrice() {
        return maxPrice;
    }

    /**
     *
     * @return
     */
    public int getPublisherID() {
        return publisherID;
    }

    /**
     *
     * @param publisherID
     */
    public void setPublisherID(int publisherID) {
        this.publisherID = publisherID;
    }

    /**
     *
     * @param authorId
     * @param categoryID
     * @param publisherID
     * @param textSearch
     * @param minPrice
     * @param maxPrice
     */
    public BookBinding(int authorId, int categoryID, int publisherID, String textSearch, double minPrice, double maxPrice) {
        this.authorId = authorId;
        this.categoryID = categoryID;
        this.publisherID = publisherID;
        this.textSearch = textSearch;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    /**
     *
     * @param authorId
     * @param categoryID
     * @param publisherID
     * @param minPrice
     * @param textSearch
     * @param maxPrice
     * @param status
     */
    public BookBinding(int authorId, int categoryID, int publisherID, double minPrice, String textSearch, double maxPrice, int status) {
        this.authorId = authorId;
        this.categoryID = categoryID;
        this.publisherID = publisherID;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.status = status;
        this.textSearch = textSearch;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "BookBinding{" + "authorId=" + authorId + ", categoryID=" + categoryID + ", publisherID=" + publisherID + ", textSearch=" + textSearch + ", minPrice=" + minPrice + ", maxPrice=" + maxPrice + '}';
    }
}
