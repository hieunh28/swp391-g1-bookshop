/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author dell
 */
public class PrimaryCategory {
    private int categoryId;
    private String categoryName;
    private Boolean status;
    private Boolean deleteFlag;

    public PrimaryCategory() {
    }

    public PrimaryCategory(int categoryId, String categoryName, Boolean status, Boolean deleteFlag) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.status = status;
        this.deleteFlag = deleteFlag;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    
    
    
}
