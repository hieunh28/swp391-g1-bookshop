/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Blog;
import Model.Constant;
import Model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bau
 */
public class BlogDAO extends DBContext {

    /**
     *
     * @param id
     * @return
     */
    public Blog getBlogById(int id) {
        UserDAO uDao = new UserDAO();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[Blog]\n"
                    + "  Where BlogId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                User user = uDao.getUserByID(rs.getInt("UserId"));

                Blog blog = new Blog();
                blog.setBlogId(id);
                blog.setUser(user);
                blog.setTitle(rs.getString("Title"));
                blog.setDatePost(rs.getDate("DatePost"));
                blog.setBlogContent(rs.getString("BlogContent"));
                blog.setBlogImage(rs.getString("BlogImage"));
                blog.setStatus(rs.getBoolean("Status"));
                blog.setDeleteFlag(rs.getBoolean("DeleteFlag"));

                return blog;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @param offset
     * @param RecordPerPage
     * @return
     */
    public ArrayList<Blog> getListBlogPagnitionByUser(int offset, int RecordPerPage) {
        ArrayList<Blog> list = new ArrayList<>();
        BlogDAO bDao = new BlogDAO();
        int count = 0;
        try {
            String sql = "SELECT BlogId\n"
                    + "  FROM [Blog]\n"
                    + "  Where DeleteFlag = 0";

            HashMap<Integer, Object> setter = new HashMap<>();

            sql += "    Order by DatePost";
            sql += "    offset ? rows\n"
                    + "  fetch next ? row only";
            setter.put(++count, offset);
            setter.put(++count, RecordPerPage);

            PreparedStatement stm = connection.prepareStatement(sql);
            for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                stm.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Blog blog = bDao.getBlogById(rs.getInt(1));
                list.add(blog);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    /**
     *
     * @return
     */
    public int getTotalBlogByUser() {
        try {
            String sql = "SELECT count(*)\n"
                    + "  FROM [Blog]\n"
                    + "  Where DeleteFlag = 0";

            HashMap<Integer, Object> setter = new HashMap<>();

            PreparedStatement stm = connection.prepareStatement(sql);
            for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                stm.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     *
     * @param offset
     * @param RecordPerPage
     * @param userId
     * @return
     */
    public ArrayList<Blog> getListBlogPagnitionByUser(int offset, int RecordPerPage, int userId) {
        ArrayList<Blog> list = new ArrayList<>();
        BlogDAO bDao = new BlogDAO();
        int count = 0;
        try {
            String sql = "SELECT BlogId\n"
                    + "  FROM [Blog]\n"
                    + "  Where DeleteFlag = 0 and UserId = ?";

            HashMap<Integer, Object> setter = new HashMap<>();
            setter.put(++count, userId);

            sql += "    Order by DatePost";
            sql += "    offset ? rows\n"
                    + "  fetch next ? row only";
            setter.put(++count, offset);
            setter.put(++count, RecordPerPage);

            PreparedStatement stm = connection.prepareStatement(sql);
            for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                stm.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Blog blog = bDao.getBlogById(rs.getInt(1));
                list.add(blog);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    /**
     *
     * @param userID
     * @return
     */
    public int getTotalBlogByUser(int userID) {
        int count = 0;
        try {
            String sql = "SELECT count(*)\n"
                    + "  FROM [Blog]\n"
                    + "  Where DeleteFlag = 0 and UserId = ?";

            HashMap<Integer, Object> setter = new HashMap<>();
            setter.put(++count, userID);

            PreparedStatement stm = connection.prepareStatement(sql);
            for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                stm.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     *
     * @param blog
     */
    public void update(Blog blog) {
        try {
            String sql = "UPDATE [dbo].[Blog]\n"
                    + "   SET [Title] = ?\n"
                    + "      ,[BlogContent] = ?\n"
                    + "      ,[BlogImage] = ?\n"
                    + " WHERE BlogId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, blog.getTitle());
            stm.setString(2, blog.getBlogContent());
            stm.setString(3, blog.getBlogImage());
            stm.setInt(4, blog.getBlogId());
            stm.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param blog
     */
    public void insertBlog(Blog blog) {
        try {
            String sql = "INSERT INTO [dbo].[Blog]\n"
                    + "           ([UserId]\n"
                    + "           ,[Title]\n"
                    + "           ,[DatePost]\n"
                    + "           ,[BlogContent]\n"
                    + "           ,[BlogImage]\n"
                    + "           ,[Status]\n"
                    + "           ,[DeleteFlag])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, blog.getUser().getUserID());
            stm.setString(2, blog.getTitle());
            stm.setDate(3, blog.getDatePost());
            stm.setString(4, blog.getBlogContent());
            stm.setString(5, blog.getBlogImage());
            stm.setBoolean(6, blog.isStatus());
            stm.setBoolean(7, blog.isDeleteFlag());
            stm.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param blogId
     */
    public void deleteBlog(int blogId) {
        try {
            String sql = "UPDATE [dbo].[Blog]\n"
                    + "   SET [DeleteFlag] = ?\n"
                    + " WHERE BlogId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setBoolean(1, Constant.DeleteTrue);
            stm.setInt(2, blogId);
            stm.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
