/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Book;
import Model.Refund;
import Model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class RefundDAO extends DBContext {

    public void insertRefund(Refund rf) {
        try {
            String sql = "INSERT INTO [dbo].[Refund]\n"
                    + "           ([BookId]\n"
                    + "           ,[UserId]\n"
                    + "           ,[Reason]\n"
                    + "           ,[PickupAddress]\n"
                    + "           ,[Status]\n"
                    + "           ,[DateRefund])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, rf.getBook().getBookId());
            stm.setInt(2, rf.getUser().getUserID());
            stm.setString(3, rf.getReason());
            stm.setString(4, rf.getPickUpAddress());
            stm.setInt(5, rf.getStatus());
            stm.setDate(6, rf.getDateRefund());

            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RefundDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList<Refund> getRefundPagnition(int offset, int RecordPerPage) {
        ArrayList<Refund> list = new ArrayList<>();
        BookDAO bDao = new BookDAO();
        UserDAO uDao = new UserDAO();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[Refund]\n"
                    + "  Order by DateRefund desc\n"
                    + "  offset ? rows\n"
                    + "  fetch next ? row only";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, offset);
            stm.setInt(2, RecordPerPage);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Book book = bDao.getBookById(rs.getInt("BookId"));
                User user = uDao.getUserByID(rs.getInt("UserId"));

                Refund refund = new Refund();
                refund.setRefundId(rs.getInt("RefundId"));
                refund.setBook(book);
                refund.setUser(user);
                refund.setReason(rs.getString("Reason"));
                refund.setPickUpAddress(rs.getString("PickupAddress"));
                refund.setStatus(rs.getInt("Status"));
                refund.setDateRefund(rs.getDate("DateRefund"));

                list.add(refund);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RefundDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public int getTotalRefund() {
        try {
            String sql = "SELECT count(*)\n"
                    + "  FROM [dbo].[Refund]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RefundDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public void changeStatus(int refundId, int status) {
        try {
            String sql = "UPDATE [dbo].[Refund]\n"
                    + "   SET [Status] = ?\n"
                    + " WHERE RefundId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, refundId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RefundDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
