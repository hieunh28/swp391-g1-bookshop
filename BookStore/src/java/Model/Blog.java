/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Bau
 */
public class Blog {
    private int blogId;
    private User user;
    private String title;
    private Date datePost;
    private String blogContent;
    private String blogImage;
    private boolean status;
    private boolean deleteFlag;
    private ArrayList<Comment> comment;

    /**
     * Constructor
     */
    public Blog() {
    }

    /**
     *
     * @param blogId
     * @param user
     * @param title
     * @param datePost
     * @param blogContent
     * @param blogImage
     * @param status
     * @param deleteFlag
     */
    public Blog(int blogId, User user, String title, Date datePost, String blogContent, String blogImage, boolean status, boolean deleteFlag) {
        this.blogId = blogId;
        this.user = user;
        this.title = title;
        this.datePost = datePost;
        this.blogContent = blogContent;
        this.blogImage = blogImage;
        this.status = status;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @param blogId
     * @param user
     * @param title
     * @param datePost
     * @param blogContent
     * @param blogImage
     * @param status
     * @param deleteFlag
     * @param comment
     */
    public Blog(int blogId, User user, String title, Date datePost, String blogContent, String blogImage, boolean status, boolean deleteFlag, ArrayList<Comment> comment) {
        this.blogId = blogId;
        this.user = user;
        this.title = title;
        this.datePost = datePost;
        this.blogContent = blogContent;
        this.blogImage = blogImage;
        this.status = status;
        this.deleteFlag = deleteFlag;
        this.comment = comment;
    }

    /**
     *
     * @return
     */
    public ArrayList<Comment> getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     */
    public void setComment(ArrayList<Comment> comment) {
        this.comment = comment;
    }
    
    /**
     *
     * @return
     */
    public int getBlogId() {
        return blogId;
    }

    /**
     *
     * @param blogId
     */
    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     */
    public Date getDatePost() {
        return datePost;
    }

    /**
     *
     * @param datePost
     */
    public void setDatePost(Date datePost) {
        this.datePost = datePost;
    }

    /**
     *
     * @return
     */
    public String getBlogContent() {
        return blogContent;
    }

    /**
     *
     * @param blogContent
     */
    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    /**
     *
     * @return
     */
    public String getBlogImage() {
        return blogImage;
    }

    /**
     *
     * @param blogImage
     */
    public void setBlogImage(String blogImage) {
        this.blogImage = blogImage;
    }

    /**
     *
     * @return
     */
    public boolean isStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public boolean isDeleteFlag() {
        return deleteFlag;
    }

    /**
     *
     * @param deleteFlag
     */
    public void setDeleteFlag(boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    
    
}
