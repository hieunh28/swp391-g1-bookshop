/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Bau
 */
public class Order {

    private int orderId;
    private int customerId;
    private User customer;
    private String customerName;
    private String customerEmail;
    private String customerPhone;
    private String customerAddress;
    private Date orderDate;
    private int status;
    private int paymentMethodId;
    private PaymentMethod paymentMethod;
    private ArrayList<OrderDetail> orderDetails;

    /**
     *
     * @return
     */
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    /**
     *
     * @param paymentMethod
     */
    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     *
     * @return
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * Constructor
     */
    public Order() {
    }

    /**
     *
     * @return
     */
    public ArrayList<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    /**
     *
     * @param orderDetails
     */
    public void setOrderDetails(ArrayList<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    /**
     *
     * @param orderId
     * @param customerId
     * @param customer
     * @param customerName
     * @param customerEmail
     * @param customerPhone
     * @param customerAddress
     * @param orderDate
     * @param status
     * @param paymentMethodId
     * @param paymentMethod
     * @param orderDetails
     */
    public Order(int orderId, int customerId, User customer, String customerName, String customerEmail, String customerPhone, String customerAddress, Date orderDate, int status, int paymentMethodId, PaymentMethod paymentMethod, ArrayList<OrderDetail> orderDetails) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.customer = customer;
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.customerPhone = customerPhone;
        this.customerAddress = customerAddress;
        this.orderDate = orderDate;
        this.status = status;
        this.paymentMethodId = paymentMethodId;
        this.paymentMethod = paymentMethod;
        this.orderDetails = orderDetails;
    }

    /**
     *
     * @param orderId
     * @param customerId
     * @param customerName
     * @param customerEmail
     * @param customerPhone
     * @param customerAddress
     * @param orderDate
     * @param status
     * @param paymentMethodId
     * @param orderDetails
     */
    public Order(int orderId, int customerId, String customerName, String customerEmail, String customerPhone, String customerAddress, Date orderDate, int status, int paymentMethodId, ArrayList<OrderDetail> orderDetails) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.customerPhone = customerPhone;
        this.customerAddress = customerAddress;
        this.orderDate = orderDate;
        this.status = status;
        this.paymentMethodId = paymentMethodId;
        this.orderDetails = orderDetails;
    }

    /**
     *
     * @return
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     *
     * @param customerId
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     *
     * @return
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     *
     * @param customerName
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     *
     * @return
     */
    public String getCustomerEmail() {
        return customerEmail;
    }

    /**
     *
     * @param customerEmail
     */
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    /**
     *
     * @return
     */
    public String getCustomerPhone() {
        return customerPhone;
    }

    /**
     *
     * @param customerPhone
     */
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    /**
     *
     * @return
     */
    public String getCustomerAddress() {
        return customerAddress;
    }

    /**
     *
     * @param customerAddress
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     *
     * @return
     */
    public Date getOrderDate() {
        return orderDate;
    }

    /**
     *
     * @param orderDate
     */
    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    /**
     *
     * @return
     */
    public int getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public int getPaymentMethodId() {
        return paymentMethodId;
    }

    /**
     *
     * @param paymentMethodId
     */
    public void setPaymentMethodId(int paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    /**
     *
     * @return
     */
    public User getCustomer() {
        return customer;
    }

    /**
     *
     * @param customer
     */
    public void setCustomer(User customer) {
        this.customer = customer;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Order{" + "orderId=" + orderId + ", customerId=" + customerId + ", customerName=" + customerName + ", customerEmail=" + customerEmail + ", customerPhone=" + customerPhone + ", customerAddress=" + customerAddress + ", orderDate=" + orderDate + ", status=" + status + ", paymentMethodId=" + paymentMethodId + ", paymentMethod=" + paymentMethod + ", orderDetails=" + orderDetails + '}';
    }

}
