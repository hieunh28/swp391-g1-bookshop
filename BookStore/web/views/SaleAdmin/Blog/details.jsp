<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <title>${requestScope.blog != null ? "Update Product" : "Create Product"}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Book-shop</title>
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
</head>
<%@include file="../../../layout/header.jsp" %>
<body>
    <%@include file="../../../layout/LeftAdminSaleBar.jsp" %>
    <section style="margin-left: 280px; height: calc(92vh); overflow-y: auto;">
        <div class="card my-2 mx-2">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <h1 class="fs-2 fw-bold">
                        Details Blog
                    </h1>
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="manage-blog" id="submitForm" enctype="multipart/form-data">
                    <div class="col">
                        <div class="row">
                            <div class="col-6">
                                <label for="category-film" class="col-form-label fw-bold">Title:</label>
                                <input type="text" class="form-control"
                                       name="title" value="${requestScope.blog.title}" required minlength="20" readonly disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label for="category-film" class="col-form-label fw-bold">Blog Content: </label>
                                <textarea class="form-control" rows="8" name="content" minlength="150" required readonly disabled>${requestScope.blog.blogContent}</textarea>
                            </div>
                        </div>
                        <div class="mt-3">
                            <label for="exampleFormControlFile1" class="col-form-label">Blog Image</label>
                            <img style="width: 150px; height: 150px; object-fit: cover; border-radius: 8px;"
                                 src="${requestScope.blog.blogImage}"
                                 alt="book-shop">
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-flex justify-content-center gap-3 col-6">
                            <c:if test="${requestScope.blog != null}">
                                <a type="button" class="btn btn-secondary btn-lg" href="manage-blog?blogId=${requestScope.blog.blogId}">
                                    Update</a>
                                </c:if>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>

<!-- Script using  -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/8d39de38b8.js" crossorigin="anonymous"></script>
<script>
</script>
