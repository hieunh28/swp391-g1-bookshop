<%@ include file="../../layout/header.jsp" %>
<head>
    <title>Home Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
</head>
<body class="overflow-x-hidden background-gray">

    <!-- Search navigation and link to cart details-->
    <%@ include file="../../layout/ShopHeaderSearchAndCart.jsp" %>

    <!--Shop menu navigation-->
    <%@ include file="../../layout/ShopNavigation.jsp" %>
    <!-- current page link  -->
    <div class="mt-3 mx-5 py-3 px-2 bg-white rounded-1 shadow-sm">
        Home / <span class="primary-color"> Cart Check Out </span>
    </div>

    <div class="row px-5 mt-4">

        <!--include left side bar-->
        <%@ include file="../../layout/LeftSideBar.jsp" %>

        <div class="col-md-7 col-lg-8">
            <div class="row g-5 mt-4">
                <div class="col-md-5 col-lg-4 order-md-last">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-dark">Your Order</span>
                        <!-- Number of items in cart  -->
                        <span class="badge bg-dark rounded-pill">${sessionScope.totalProduct}</span>
                    </h4>
                    <c:if test="${overQuantity == true}">
                        <span style="color: red">Some product not enough quantity and quantity is changed!</span>
                    </c:if>
                    <ul class="list-group mb-3">
                        <c:set var="total" value="0"></c:set>
                        <c:forEach items="${requestScope.items}" var="c">
                            <div>
                                <li class="list-group-item d-flex justify-content-between align-items-center lh-sm">
                                    <div class="d-flex align-items-center gap-4">
                                        <img src="${c.book.images.get(0).url}"
                                             class=""
                                             style="width: 50px; height: 50px; object-fit: cover; border-radius: 8px;" />
                                        <span class="product-thumbnail__quantity">${c.quantity}</span>
                                        <div>
                                            <h6 class="my-0">${c.book.title}</h6>
                                            ${c.book.category.categoryName}
                                        </div>
                                    </div>
                                    <span class="text-muted">
                                        ${c.book.price * c.quantity}$
                                    </span>
                                </li>
                            </div>
                            <c:set var="total" value="${total + c.book.price * c.quantity}"></c:set>
                        </c:forEach>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Total </span>
                            <strong class="pr-0 mr-0" id="totalPrice">${total}$</strong>
                        </li>
                    </ul>
                </div>
                <div class="col-md-7 col-lg-8 row g-5 mt-0">
                    <form class="row" action="cart-check-out" method="post">
                        <div class="col-lg-6 col-12">
                            <h4 class="mb-3">Order</h4>
                            <div class="row g-3">
                                <div class="col-12">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" class="form-control" name="email" placeholder="you@example.com" oninput="validateEmail(this)" value="${sessionScope.account.email}" readonly="" disabled="" required>
                                </div>
                                <div class="col-12">
                                    <label for="username" class="form-label">Phone number</label>
                                    <input class="form-control mb-3" type="text" name="phone" pattern="[0]{1}[0-9]{9}" 
                                           title="Please enter a valid phone number! (Ex: 0987654321)" required value="${sessionScope.account.phone}">
                                    <span id="error-phone" style="color: red; display: none;"></span>
                                </div>
                                <div class="col-12">
                                    <label for="username" class="form-label">Full Name</label>
                                    <input id="input-fullName" type="text" name="fullName" required maxlength="255" oninput="validateName(this)"
                                           class="form-control mb-3" placeholder="Nh?p H? v� T�n" value="${sessionScope.account.fullName}"
                                           aria-label="Username">
                                    <span id="error-fullName" style="color: red; display: none;"></span>
                                </div>

                                <div class="col-12">
                                    <label for="address" class="form-label">Shipping Address</label>
                                    <input type="text" value="${sessionScope.account.address}" class="form-control" name="address" id="addressID" placeholder="Your order address" required>
                                    <span style="color: red;display: none" id="error-address"></span>
                                </div>
                            </div>
                            <hr class="my-4">
                            <div class="p-2 d-flex justify-content-between align-items-center gap-2">
                                <a href="home" style="color: black; text-decoration: none;">
                                    <i class="fa-solid fa-arrow-left-long"></i>
                                    Back to home
                                </a>
                                <button type="submit" class="btn btn-dark">Place Order</button>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <h4 class="mb-3">Extra fee</h4>
                            <div class="d-flex justify-content-between align-items-center mb-4"
                                 style="padding: 8px 12px; border: 1px solid #00000020; border-radius: 9px;">
                                <div class="d-flex gap-2 align-items-center">
                                    <input checked type="radio" class="form-check-input"
                                           id="exampleRadio1">
                                    <span class="d-flex justify-content-between align-items-center" for="exampleRadio1">
                                        Delivery fee
                                </div>
                                <div style="font-weight: bold;">5$</div>
                            </div>
                            <h4 class="mb-3">Payment</h4>
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <div class="p-3 d-flex align-items-center justify-content-between gap-3"
                                         id="headingOne" data-bs-toggle="collapse" data-bs-target="#collapseOne">
                                        <div class="d-flex gap-2">
                                            <input class="form-check-input" type="radio" name="payment" id="bank"
                                                   value="2">
                                            <p class="mb-0">Banking</p>
                                        </div>
                                        <i class="fa-solid fa-money-check-dollar fa-xl"></i>
                                    </div>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                         aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body" style="background-color: #f8f8f8;">
                                            Techcombank (TCB) <br />
                                            STK: 777777 <br />
                                            Nguyen Huu Hieu <br /><br /> <br />
                                            Transfer content: "Your order phone number"
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <div class="p-3 d-flex align-items-center justify-content-between gap-3"
                                         id="headingTwo" data-bs-toggle="collapse" data-bs-target="#collapseTwo">
                                        <div class="d-flex gap-2">
                                            <input class="form-check-input" type="radio" name="payment" id="cod"
                                                   value="1" checked>
                                            <p class="mb-0">Cash On Delivery (COD)</p>
                                        </div>
                                        <i class="fa-solid fa-money-check-dollar fa-xl"></i>
                                    </div>
                                    <div id="collapseTwo" class="accordion-collapse collapse"
                                         aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                        <div class="accordion-body"
                                             style="background-color: #f8f8f8; padding-top: 20px; padding-bottom: 20px;">
                                            You only need to pay when receive order!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div data-include="footer"></div>
    <!-- Script using  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
    crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/8d39de38b8.js" crossorigin="anonymous"></script>
    <script>
        $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000,
        });
    </script>
</body>
