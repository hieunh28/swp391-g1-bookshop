/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Book;
import Model.Cart;
import Model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class CartDAO extends DBContext {

    /**
     *
     * @param userId
     * @param productId
     * @param quantity
     */
    public void addToCart(int userId, int productId, int quantity) {
        try {
            // First, check if the item is already in the cart for the user
            String checkSQL = "SELECT * FROM [Cart] WHERE CustomerId = ? AND ProductId = ?";
            PreparedStatement checkStm = connection.prepareStatement(checkSQL);
            checkStm.setInt(1, userId);
            checkStm.setInt(2, productId);
            ResultSet checkRs = checkStm.executeQuery();

            if (checkRs.next()) {
                // If the item is already in the cart, update the quantity
                String updateSQL = "UPDATE [Cart] SET Quantity = ? WHERE CustomerId = ? AND ProductId = ?";
                PreparedStatement updateStm = connection.prepareStatement(updateSQL);
                updateStm.setInt(1, checkRs.getInt("quantity") + quantity);
                updateStm.setInt(2, userId);
                updateStm.setInt(3, productId);
                updateStm.executeUpdate();
            } else {
                // If the item is not in the cart, insert a new record
                String insertSQL = "INSERT INTO [Cart] (CustomerId, ProductId, Quantity) VALUES (?, ?, ?)";
                PreparedStatement insertStm = connection.prepareStatement(insertSQL);
                insertStm.setInt(1, userId);
                insertStm.setInt(2, productId);
                insertStm.setInt(3, quantity);
                insertStm.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param userId
     */
    public void DeleteCartCustomer(int userId) {
        try {
            String sql = "DELETE FROM [Cart] WHERE CustomerId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        CartDAO cDao = new CartDAO();
        System.out.println(cDao.getCartByCustomer(3).get(0).getQuantity());
    }

    /**
     *
     * @param userID
     * @return
     */
    public ArrayList<Cart> getCartByCustomer(int userID) {
        UserDAO uDao = new UserDAO();
        BookDAO bDao = new BookDAO();
        ArrayList<Cart> list = new ArrayList<>();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[Cart]\n"
                    + "  Where CustomerId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User customer = uDao.getUserByID(rs.getInt("CustomerId"));
                Book book = bDao.getBookById(rs.getInt("ProductId"));

                Cart c = new Cart(rs.getInt("CartId"), customer, book, rs.getInt("Quantity"));
                list.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    /**
     *
     * @param cartId
     * @param quantityChange
     */
    public void changeProductQuantity(int cartId, int quantityChange) {
        try {
            //sql de up date cart
            String sql = "UPDATE [dbo].[Cart]\n"
                    + "   SET [Quantity] = Quantity + ?\n"
                    + " WHERE CartId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            //gan  gia tri cho Quantity
            stm.setInt(1, quantityChange);
            //gan gia tri cho CartId
            stm.setInt(2, cartId);
            //thuc hien cau lenh sql
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param cartId
     */
    public void deleteCart(int cartId) {
        try {
            //sql de xoa cart
            String sql = "DELETE FROM [dbo].[Cart]\n"
                    + "      WHERE CartId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            //gan gia tri cho CartId
            stm.setInt(1, cartId);
            //thuc hien cau lenh sql
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param cartId
     * @param quantity
     */
    public void updateQuantity(int cartId, int quantity) {
        try {
            String sql = "UPDATE [dbo].[Cart]\n"
                    + "   SET [Quantity] = ?\n"
                    + " WHERE CartId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, quantity);
            stm.setInt(2, cartId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int getTotalQuantityInCart(int userId) {
    int totalQuantity = 0;
    try {
        String sql = "SELECT COUNT(DISTINCT ProductId) AS TotalQuantity FROM [dbo].[Cart] WHERE CustomerId = ?";
        PreparedStatement stm = connection.prepareStatement(sql);
        stm.setInt(1, userId);
        ResultSet rs = stm.executeQuery();
        if (rs.next()) {
            totalQuantity = rs.getInt("TotalQuantity");
        }
    } catch (SQLException ex) {
        Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
    }
    return totalQuantity;
}

    
    

}
