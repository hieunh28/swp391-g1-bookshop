/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.User;

import DAL.BookDAO;
import DAL.CartDAO;
import DAL.OrderDAO;
import DAL.OrderDetailsDAO;
import Model.Cart;
import Model.Constant;
import Model.Order;
import Model.OrderDetail;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Bau
 */

public class CartCheckOutController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CartCheckOutController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CartCheckOutController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CartDAO cDao = new CartDAO();
        String url = request.getHeader("Referer");
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");
        if (account != null) {
            ArrayList<Cart> carts;
            carts = cDao.getCartByCustomer(account.getUserID());
            if (carts.isEmpty()) {
                session.setAttribute("msg", "Cart empty! Add book to check out!");
                response.sendRedirect(url);
            } else {
                for (Cart cart : carts) {
                    if (cart.getQuantity() > cart.getBook().getQuantity()) {
                        request.setAttribute("overQuantity", true);
                        cart.setQuantity(cart.getBook().getQuantity());
                        cDao.updateQuantity(cart.getCartId(), cart.getQuantity());
                    } else if (cart.getBook().getQuantity() == 0) {
                        cDao.deleteCart(cart.getCartId());
                    }
                }
                request.setAttribute("items", carts);
                request.getRequestDispatcher("/views/User/cartCheckOut.jsp").forward(request, response);
            }
        } else {
            session.setAttribute("msg", "You need to login to check out your cart!");
            response.sendRedirect(url);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String name = request.getParameter("fullName");
        String address = request.getParameter("address");
        int payment = Integer.parseInt(request.getParameter("payment"));

        //get current account
        HttpSession session = request.getSession();
        User account = (User) session.getAttribute("account");

        //get all cart of customer
        CartDAO cDao = new CartDAO();
        ArrayList<Cart> carts = cDao.getCartByCustomer(account.getUserID());

        //get current date
        Date date = Date.valueOf(LocalDate.now());

        Order order = new Order();
        order.setCustomerId(account.getUserID());
        order.setCustomerName(name);
        order.setCustomerEmail(email);
        order.setCustomerPhone(phone);
        order.setCustomerAddress(address);
        order.setOrderDate(date);
        order.setStatus(Constant.OrderPending);
        order.setPaymentMethodId(payment);

        //insert cart to db
        OrderDAO oDao = new OrderDAO();
        oDao.insertOrder(order);

        int lastOrderId = oDao.getLastOrderId();

        //insert order details
        OrderDetailsDAO odDao = new OrderDetailsDAO();
        for (Cart cart : carts) {
            OrderDetail od = new OrderDetail();
            od.setOrderId(lastOrderId);
            od.setBookId(cart.getBook().getBookId());
            od.setQuantity(cart.getQuantity());
            od.setIsRated(Boolean.FALSE);
            odDao.inset(od);
        }

        //clear cart of customer
        cDao.DeleteCartCustomer(account.getUserID());

        //update quantity of product
        BookDAO bDao = new BookDAO();
        for (Cart cart : carts) {
            bDao.UpdateBookQuantity(cart.getBook().getBookId(), -cart.getQuantity());
        }

        request.getSession().setAttribute("msg", "Place order successfull!");
        response.sendRedirect("home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
