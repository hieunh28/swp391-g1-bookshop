<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Book-shop</title>
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous" />
        <link rel="stylesheet" type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
        <link rel="stylesheet" type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
    </head>

    <body class="overflow-x-hidden background-gray">
        <%@ include file="../layout/header.jsp" %>
        <!-- Search navigation and link to cart details-->
        <%@ include file="/layout/ShopHeaderSearchAndCart.jsp" %>
        <!--Shop menu navigation-->
        <%@ include file="/layout/ShopNavigation.jsp" %>
        <!-- current page link  -->
        <div class="mt-3 mx-5 py-3 px-2 bg-white rounded-1 shadow-sm">
            Home / <span class="primary-color"> Blog Details </span>
        </div>

        <div class="row px-5 mt-4">
            <!--include left side bar-->
            <%@ include file="../layout/LeftSideBar.jsp" %>
            <div class="col-9 d-flex flex-column gap-5 mb-5">
                <!-- PRODUCT DETAIL -->
                <section>
                    <div class="row">
                        <div class="col-md-4 col-12">
                            <div class="slider slider-show-image">
                                <!-- list all image you have here -->
                                <img style="width: 100%; height: 450px; object-fit: cover; border-radius: 8px;"
                                     src="${blog.blogImage}"
                                     alt="book-shop">

                            </div>
                            <div class="slider slider-list-image">
                                <!-- list all image you have here -->
                                <img style="width: 100%; height: 150px; object-fit: cover; border-radius: 8px;"
                                     src="${blog.blogImage}"
                                     alt="book-shop">
                            </div>
                        </div>
                        <div class="col-md-8 col-12">
                            <h3 style="font-weight: 400; margin-bottom: 14px;">${blog.title}</h3>
                            <div style="height: 1px; width: 100%; background-color: #33333330;"></div>
                            <div class="mt-3 mb-4">
                                <div class="row">
                                    <div class="col-6">
                                        <p class="mb-0"><span style="font-weight: bold;">Author: </span> ${blog.user.fullName}</p>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6">
                                        <p class="mb-0"><span style="font-weight: bold;">Date: </span> ${blog.datePost}</p>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 1px; width: 100%; background-color: #33333330;"></div>
                            <p class="mb-0 mt-1"><span style="font-weight: bold;">Content: </span> ${blog.blogContent}</p>
                        </div>
                    </div>
                </section>
                <hr>

               

            </div>
        </div>
        <div data-include="footer"></div>
        <!-- Script using  -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/8d39de38b8.js" crossorigin="anonymous"></script>
        <script>
            $('.slider-show-image').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-list-image'
            });
            $('.slider-list-image').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slider-show-image',
                focusOnSelect: true
            });
        </script>
    </body>

</html>