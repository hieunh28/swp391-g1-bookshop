/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Admin
 */
public class Favourite {
    private int favouriteId;
    private int bookId;
    private int userId;
    private User user;
    private Book book;

    public Favourite(int favouriteId, int bookId, int userId, User user, Book book) {
        this.favouriteId = favouriteId;
        this.bookId = bookId;
        this.userId = userId;
        this.user = user;
        this.book = book;
    }

    public Favourite() {
    }

    public int getFavouriteId() {
        return favouriteId;
    }

    public void setFavouriteId(int favouriteId) {
        this.favouriteId = favouriteId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "Favourite{" + "favouriteId=" + favouriteId + ", bookId=" + bookId + ", userId=" + userId + ", user=" + user + ", book=" + book + '}';
    }
    
    
}
