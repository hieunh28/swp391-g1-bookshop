/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Blog;
import Model.Comment;
import Model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bau
 */
public class CommentDAO extends DBContext {

    /**
     *
     * @param cmt
     */
    public void insertComment(Comment cmt) {
        try {
            String sql = "INSERT INTO [dbo].[Comment]\n"
                    + "           ([Value]\n"
                    + "           ,[DateTime]\n"
                    + "           ,[UserId]\n"
                    + "           ,[BlogId])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cmt.getValue());
            stm.setTimestamp(2, cmt.getDatetime());
            stm.setInt(3, cmt.getUser().getUserID());
            stm.setInt(4, cmt.getBlog().getBlogId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param cmt
     */
    public void insertReply(Comment cmt) {
        try {
            String sql = "INSERT INTO [dbo].[Comment]\n"
                    + "           ([Value]\n"
                    + "           ,[DateTime]\n"
                    + "           ,[UserId]\n"
                    + "           ,[ParentId])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cmt.getValue());
            stm.setTimestamp(2, cmt.getDatetime());
            stm.setInt(3, cmt.getUser().getUserID());
            stm.setInt(4, cmt.getParent().getCommentId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param postId
     * @return
     */
    public ArrayList<Comment> getCommentByPost(int postId) {
        ArrayList<Comment> list = new ArrayList<>();
        ArrayList<Comment> children;
        UserDAO uDao = new UserDAO();
        BlogDAO bDao = new BlogDAO();
        CommentDAO cDao = new CommentDAO();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[Comment]\n"
                    + "  Where BlogId = ?\n"
                    + "  Order by DateTime desc";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, postId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User user;
                user = uDao.getUserByID(rs.getInt("UserId"));

                Blog blog;
                blog = bDao.getBlogById(rs.getInt("BlogId"));

                children = cDao.getCommentChildByCmtId(rs.getInt("CommentId"));

                Comment cmt = new Comment();
                cmt.setCommentId(rs.getInt("CommentId"));
                cmt.setValue(rs.getString("Value"));
                cmt.setDatetime(rs.getTimestamp("DateTime"));
                cmt.setUser(user);
                cmt.setBlog(blog);
                cmt.setChildren(children);

                list.add(cmt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    /**
     *
     * @param cmtId
     * @return
     */
    public ArrayList<Comment> getCommentChildByCmtId(int cmtId) {
        ArrayList<Comment> list = new ArrayList<>();
        UserDAO uDao = new UserDAO();
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[Comment]\n"
                    + "  Where ParentId = ?\n"
                    + "  Order by DateTime desc";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cmtId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User user;
                user = uDao.getUserByID(rs.getInt("UserId"));

                Comment cmt = new Comment();
                cmt.setCommentId(rs.getInt("CommentId"));
                cmt.setValue(rs.getString("Value"));
                cmt.setDatetime(rs.getTimestamp("DateTime"));
                cmt.setUser(user);

                list.add(cmt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    /**
     *
     * @param cmtId
     */
    public void deleteComment(int cmtId) {
        try {
            String sql = "DELETE FROM [dbo].[Comment]\n"
                    + "      WHERE CommentId = ? or ParentId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cmtId);
            stm.setInt(2, cmtId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
