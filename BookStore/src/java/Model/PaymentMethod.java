/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Bau
 */
public class PaymentMethod {
    private int paymentMethodId;
    private String paymentMethodName;
    private Boolean status;
    private Boolean deleteFlag;

   /**
     *
     * Constructor
     */
    public PaymentMethod() {
    }

    /**
     *
     * @param paymentMethodId
     * @param paymentMethodName
     * @param status
     * @param deleteFlag
     */
    public PaymentMethod(int paymentMethodId, String paymentMethodName, Boolean status, Boolean deleteFlag) {
        this.paymentMethodId = paymentMethodId;
        this.paymentMethodName = paymentMethodName;
        this.status = status;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    public int getPaymentMethodId() {
        return paymentMethodId;
    }

    /**
     *
     * @param paymentMethodId
     */
    public void setPaymentMethodId(int paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    /**
     *
     * @return
     */
    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    /**
     *
     * @param paymentMethodName
     */
    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    /**
     *
     * @return
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    /**
     *
     * @param deleteFlag
     */
    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "PaymentMethod{" + "paymentMethodId=" + paymentMethodId + ", paymentMethodName=" + paymentMethodName + ", status=" + status + ", deleteFlag=" + deleteFlag + '}';
    }
    
    
}
