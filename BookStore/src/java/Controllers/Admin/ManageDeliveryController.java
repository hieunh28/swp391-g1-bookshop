/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Admin;

import DAL.DeliveryDAO;
import Model.Constant;
import Model.Delivery;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;

/**
 *
 * @author dell
 */
public class ManageDeliveryController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageDeliveryController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageDeliveryController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         DeliveryDAO dDao = new DeliveryDAO();
        if (request.getParameter("deliveryId") != null) {
            Delivery delivery = dDao.getDeliveryById(Integer.parseInt(request.getParameter("deliveryId")));
            request.setAttribute("delivery", delivery);
        }
        request.getRequestDispatcher("/views/Admin/Delivery/create.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        DeliveryDAO dDao = new DeliveryDAO();
        Delivery delivery = new Delivery();
        int deliveryId;
        String deliveryName;
        double shippingFee;
         String contactNumber; // New field

        switch (action) {
            case Constant.Create:
                Delivery deli = new Delivery();

                deli.setDeliveryName(request.getParameter("deliveryName"));
                deli.setShippingFee(Double.parseDouble(request.getParameter("shippingFee")));
                deli.setStatus(Constant.StatusActive);
                deli.setDeleteFlag(Constant.DeleteFalse);
                deli.setContactNumber(request.getParameter("contactNumber")); // Set the new field
                dDao.insertDelivery(deli);
                request.getSession().setAttribute("msg", "Create successful!");
                break;

            case Constant.Update:
                // get values sent from the client
                deliveryId = Integer.parseInt(request.getParameter("deliveryId"));
                deliveryName = request.getParameter("deliveryName");
                shippingFee = Double.parseDouble(request.getParameter("shippingFee"));
                contactNumber = request.getParameter("contactNumber"); // Get the new field

                delivery.setDeliveryId(deliveryId);
                delivery.setDeliveryName(deliveryName);
                delivery.setShippingFee(shippingFee);
                delivery.setContactNumber(contactNumber); // Set the new field

                dDao.updateDelivery(delivery);
                break;
                
            case Constant.Delete:
                //get value is send from client
                deliveryId = Integer.parseInt(request.getParameter("deliveryId"));
                dDao.deleteDelivery(deliveryId);
                request.getSession().setAttribute("msg", "Delete successful!");
                break;
        }
        response.sendRedirect("list-delivery");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
