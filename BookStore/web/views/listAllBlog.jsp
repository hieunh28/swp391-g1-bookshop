<head>
    <title>Products</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" />
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
</head>
<body class="overflow-x-hidden background-gray">

    <%@ include file="../layout/header.jsp" %>
    <!-- Search navigation and link to cart details-->
    <%@ include file="/layout/ShopHeaderSearchAndCart.jsp" %>
    <!--Shop menu navigation-->
    <%@ include file="/layout/ShopNavigation.jsp" %>
    <!-- current page link  -->
    <div class="mt-3 mx-5 py-3 px-2 bg-white rounded-1 shadow-sm">
        Home / <span class="primary-color"> Blogs </span>
    </div>
    <div class="row px-5 mt-4">

        <!--include left side bar-->
        <%@ include file="../layout/LeftSideBar.jsp" %>

        <div class="col-9 d-flex flex-column gap-5">
            <!-- LIST PRODUCTS -->
            <section>
                <div class="fs-3 text-start">
                    BLOGS
                </div>
                <div class="mt-3 row">
                    <section>
                        <div class="mt-3 row">
                            <c:forEach items="${items}" var="item">
                                <div class="d-grid col-4 mt-2">
                                    <div class="border shadow-sm bg-white px-4 py-3 d-flex flex-column gap-3 align-items-center">
                                        <div class="fs-6 fw-light primary-color">
                                            ${item.datePost}
                                        </div>
                                        <div class="fs-4">
                                            ${item.title.substring(0,15)}...
                                        </div>
                                        <img class="rounded" style="height: 200px; object-fit: cover; max-width: 260px;"
                                             src="${item.blogImage}" alt="">
                                        <div class="fs-6 fw-light text-black">
                                            ${item.blogContent.substring(0,150)}...
                                        </div>
                                        <form action="blog-details" method="get">
                                            <input type="hidden" name="blogId" value="${item.blogId}">
                                            <button type="submit" class="btn btn-primary btn-lg">READ MORE</button>
                                        </form>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </section>
                    <c:if test="${items.size() == 0}">
                        <div class="d-flex justify-content-center">
                            <h4>Not found any blog!</h4>
                        </div>
                    </c:if>
                </div>
                <!--Pagination of item (6 item each page)--> 
                <c:if test="${totalPage > 0}">
                    <div class="d-flex justify-content-center mt-1">
                        <nav aria-label="Page navigation example col-12">
                            <ul class="pagination">
                                <%--For displaying Previous link except for the 1st page --%>
                                <c:if test="${currentPage != 1}">
                                    <li class="page-item">
                                        <a class="page-link" href="list-all-blog?${queryString}page=${currentPage - 1}" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                </c:if>

                                <%--For displaying Page numbers. The when condition does not display
                                            a link for the current page--%>
                                <c:forEach begin="1" end="${totalPage}" var="i">
                                    <c:choose>
                                        <c:when test="${currentPage eq i}"> 
                                            <li class="page-item"><a class="page-link bg-light">${i}</a></li>
                                            </c:when>
                                            <c:otherwise>
                                            <li class="page-item"><a class="page-link" href="list-all-blog?${queryString}page=${i}">${i}</a></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>

                                <%--For displaying Next link --%>
                                <c:if test="${currentPage lt totalPage}">
                                    <li class="page-item">
                                        <a class="page-link" href="list-all-blog?${queryString}page=${currentPage + 1}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </c:if>
                            </ul>
                        </nav>
                    </div>
                </c:if>
            </section>
        </div>
    </div>
    <div data-include="footer"></div>
    <!-- Script using  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
    crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/8d39de38b8.js" crossorigin="anonymous"></script>
    <script>
        $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000,
        });

        var querySearch = '${queryString}';

        function findAuthor() {
            var authorId = document.getElementById('author-select').value;
            window.location.href = 'listAllBook?${queryString.replace(authorSearch,"")}authorId=' + authorId;
        }

        function findPublisher() {
            var publisherId = document.getElementById('publisher-select').value;
            window.location.href = 'listAllBook?${queryString.replace(publisherSearch,"")}publisherId=' + publisherId;
        }

        function sortBy() {
            var sortBy = document.getElementById('sort-select').value;
            window.location.href = 'listAllBook?${queryString.replace(sortOption,"")}sortBy=' + sortBy;
        }
    </script>
</body>