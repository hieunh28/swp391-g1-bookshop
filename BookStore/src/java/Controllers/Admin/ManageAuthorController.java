/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Admin;

import DAL.AuthorDAO;
import Model.Author;
import Model.Constant;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;

/**
 *
 * @author Bau
 */
public class ManageAuthorController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateAuthorController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateAuthorController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AuthorDAO aDao = new AuthorDAO();
        if (request.getParameter("authorId") != null) {
            Author author = aDao.getAuthorById(Integer.parseInt(request.getParameter("authorId")));
            request.setAttribute("author", author);
        }
        request.getRequestDispatcher("views/Admin/Author/create.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        AuthorDAO aDao = new AuthorDAO();
        Author author = new Author();
        int authorId;
        String name;
        Date dob;
        switch (action) {
            case Constant.Create:
                //get value is send from client
                name = request.getParameter("name");
                dob = Date.valueOf(request.getParameter("dob"));

                author.setAuthorName(name);
                author.setDob(dob);
                author.setStatus(Constant.StatusActive);
                author.setDeleteFlag(Constant.DeleteFalse);

                aDao.insertAuthor(author);
                break;
            case Constant.Update:
                //get value is send from client
                authorId = Integer.parseInt(request.getParameter("authorId"));
                name = request.getParameter("name");
                dob = Date.valueOf(request.getParameter("dob"));

                author.setAuthorId(authorId);
                author.setAuthorName(name);
                author.setDob(dob);

                aDao.updateAuthor(author);
                break;
        }
        response.sendRedirect("authorManage");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
