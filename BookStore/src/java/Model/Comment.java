/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author Bau
 */
public class Comment {
    private int commentId;
    private String value;
    private Timestamp datetime;
    private User user;
    private Blog blog;
    private Comment parent;
    private ArrayList<Comment> children;

    /**
     * Constructor
     */
    public Comment() {
    }

    /**
     *
     * @param commentId
     * @param value
     * @param datetime
     * @param user
     * @param blog
     * @param parent
     * @param childrent
     */
    public Comment(int commentId, String value, Timestamp datetime, User user, Blog blog, Comment parent, ArrayList<Comment> childrent) {
        this.commentId = commentId;
        this.value = value;
        this.datetime = datetime;
        this.user = user;
        this.blog = blog;
        this.parent = parent;
        this.children = childrent;
    }

    /**
     *
     * @return
     */
    public int getCommentId() {
        return commentId;
    }

    /**
     *
     * @param commentId
     */
    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    /**
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public Timestamp getDatetime() {
        return datetime;
    }

    /**
     *
     * @param datetime
     */
    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    public Blog getBlog() {
        return blog;
    }

    /**
     *
     * @param blog
     */
    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    /**
     *
     * @return
     */
    public Comment getParent() {
        return parent;
    }

    /**
     *
     * @param parent
     */
    public void setParent(Comment parent) {
        this.parent = parent;
    }

    /**
     *
     * @return
     */
    public ArrayList<Comment> getChildren() {
        return children;
    }

    /**
     *
     * @param children
     */
    public void setChildren(ArrayList<Comment> children) {
        this.children = children;
    }
    
    
}
