/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Bau
 */
public class User {

    private int userID;
    private String fullName;
    private String email;
    private String phone;
    private String password;
    private Date dob;
    private String address;
    private String avatar;
    private int gender;
    private Role role;
    private boolean status;
    private String description;
    private ArrayList<Order> oders;
    private ArrayList<RateBook> comments;

    /**
     *
     * @return
     */
    public ArrayList<RateBook> getComments() {
        return comments;
    }

    /**
     *
     * @param comments
     */
    public void setComments(ArrayList<RateBook> comments) {
        this.comments = comments;
    }
    
    /**
     *
     * @return
     */
    public ArrayList<Order> getOders() {
        return oders;
    }

    /**
     *
     * @param oders
     */
    public void setOders(ArrayList<Order> oders) {
        this.oders = oders;
    }

    /**
     *
     * Constructor
     */
    public User() {
    }

    /**
     *
     * @param userID
     * @param fullName
     * @param email
     * @param phone
     * @param dob
     * @param address
     * @param avatar
     * @param role
     * @param status
     * @param description
     * @param gender
     */
    public User(int userID, String fullName, String email, String phone, Date dob, String address, String avatar, Role role, boolean status, String description, int gender) {
        this.userID = userID;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.dob = dob;
        this.address = address;
        this.avatar = avatar;
        this.role = role;
        this.status = status;
        this.description = description;
        this.gender = gender;
    }

    /**
     *
     * @param customerId
     * @param fullName
     * @param email
     * @param phone
     * @param dob
     * @param address
     * @param avatar
     * @param role
     * @param status
     * @param description
     */
    public User(int customerId, String fullName, String email, String phone, Date dob, String address, String avatar, Role role, boolean status, String description) {
        this.userID = customerId;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.dob = dob;
        this.address = address;
        this.avatar = avatar;
        this.role = role;
        this.status = status;
        this.description = description;
    }

    /**
     *
     * @param userID
     * @param fullName
     * @param email
     * @param phone
     * @param dob
     * @param address
     * @param avatar
     * @param gender
     * @param role
     * @param status
     * @param description
     */
    public User(int userID, String fullName, String email, String phone, Date dob, String address, String avatar, int gender, Role role, boolean status, String description) {
        this.userID = userID;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.dob = dob;
        this.address = address;
        this.avatar = avatar;
        this.role = role;
        this.status = status;
        this.description = description;
        this.gender = gender;
    }

    /**
     *
     * @param userID
     * @param fullName
     * @param email
     * @param phone
     * @param password
     * @param dob
     * @param address
     * @param avatar
     * @param role
     * @param status
     * @param description
     * @param gender
     */
    public User(int userID, String fullName, String email, String phone, String password, Date dob, String address, String avatar, Role role, boolean status, String description, int gender) {
        this.userID = userID;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.dob = dob;
        this.address = address;
        this.avatar = avatar;
        this.role = role;
        this.status = status;
        this.description = description;
        this.gender = gender;
    }

    /**
     *
     * @return
     */
    public int getUserID() {
        return userID;
    }

    /**
     *
     * @param userID
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }

    /**
     *
     * @return
     */
    public String getFullName() {
        return fullName;
    }

    /**
     *
     * @param fullName
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     *
     * @return
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public Date getDob() {
        return dob;
    }

    /**
     *
     * @param dob
     */
    public void setDob(Date dob) {
        this.dob = dob;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     *
     * @param avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     *
     * @return
     */
    public Role getRole() {
        return role;
    }

    /**
     *
     * @param role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     *
     * @return
     */
    public boolean isStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public int getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(int gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "User{" + "userID=" + userID + ", fullName=" + fullName + ", phone=" + phone + ", email=" + email + ", password=" + password + ", dob=" + dob + ", address=" + address + ", avatar=" + avatar + ", role=" + role + ", status=" + status + ", description=" + description + ", gender=" + gender + '}';
    }

}
