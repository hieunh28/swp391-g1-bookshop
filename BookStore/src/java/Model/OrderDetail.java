/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Bau
 */
public class OrderDetail {
    private int orderDetailId;
    private int orderId;
    private int bookId;
    private int quantity;
    private Boolean isRated;
    private Order order;
    private Book book;

    /**
     *
     * @return
     */
    public Book getBook() {
        return book;
    }

    /**
     *
     * @param book
     */
    public void setBook(Book book) {
        this.book = book;
    }
    
    /**
     *
     * Constructor
     */
    public OrderDetail() {
    }

    /**
     *
     * @param orderDetailId
     * @param orderId
     * @param bookId
     * @param quantity
     * @param IsRated
     * @param order
     */
    public OrderDetail(int orderDetailId, int orderId, int bookId, int quantity, Boolean IsRated, Order order) {
        this.orderDetailId = orderDetailId;
        this.orderId = orderId;
        this.bookId = bookId;
        this.quantity = quantity;
        this.isRated = IsRated;
        this.order = order;
    }

    /**
     *
     * @return
     */
    public int getOrderDetailId() {
        return orderDetailId;
    }

    /**
     *
     * @param orderDetailId
     */
    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    /**
     *
     * @return
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return
     */
    public int getBookId() {
        return bookId;
    }

    /**
     *
     * @param bookId
     */
    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    /**
     *
     * @return
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return
     */
    public Boolean getIsRated() {
        return isRated;
    }

    /**
     *
     * @param IsRated
     */
    public void setIsRated(Boolean IsRated) {
        this.isRated = IsRated;
    }

    /**
     *
     * @return
     */
    public Order getOrder() {
        return order;
    }

    /**
     *
     * @param order
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "OrderDetail{" + "orderDetailId=" + orderDetailId + ", orderId=" + orderId + ", bookId=" + bookId + ", quantity=" + quantity + ", isRated=" + isRated + ", order=" + order + '}';
    }
    
    
}
