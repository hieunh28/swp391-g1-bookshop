/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Bau
 */
public class BookImage {
    private int imageId;
    private int bookId;
    private String url;
    private Boolean deleteFlag;

    /**
     *
     * @param bookId
     * @param url
     */
    public BookImage(int bookId, String url) {
        this.bookId = bookId;
        this.url = url;
    }

    /**
     *
     * @param imageId
     * @param bookId
     * @param url
     * @param deleteFlag
     */
    public BookImage(int imageId, int bookId, String url, Boolean deleteFlag) {
        this.imageId = imageId;
        this.bookId = bookId;
        this.url = url;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * Constructor
     */
    public BookImage() {
    }

    /**
     *
     * @return
     */
    public int getImageId() {
        return imageId;
    }

    /**
     *
     * @param imageId
     */
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    /**
     *
     * @return
     */
    public int getBookId() {
        return bookId;
    }

    /**
     *
     * @param bookId
     */
    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    /**
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     */
    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    /**
     *
     * @param deleteFlag
     */
    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    
}
