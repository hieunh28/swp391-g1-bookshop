package DAL;

import Model.Delivery;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DeliveryDAO extends DBContext {

    public ArrayList<Delivery> getAllDeliveryPagnition(int offset, int RecordPerPage) {
        ArrayList<Delivery> list = new ArrayList<>();
        int count = 0;
        try {
            String sql = "SELECT *\n"
                    + "  FROM [Delivery]\n"
                    + "  WHERE DeleteFlag = 0\n"
                    + "  ORDER BY DeliveryId\n"
                    + "  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

            HashMap<Integer, Object> setter = new HashMap<>();
            setter.put(++count, offset);
            setter.put(++count, RecordPerPage);

            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                    ps.setObject(entry.getKey(), entry.getValue());
                }
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        Delivery d = new Delivery();
                        d.setDeliveryId(rs.getInt("DeliveryId"));
                        d.setDeliveryName(rs.getString("DeliveryName"));
                        d.setStatus(rs.getBoolean("Status"));
                        d.setShippingFee(rs.getDouble("ShippingFee"));
                        d.setContactNumber(rs.getString("ContactNumber")); // Include the new field
                        list.add(d);
                    }
                }
            }
        } catch (SQLException e) {
            // Handle SQLException
        }
        return list;
    }

    public int getTotalDelivery() {
        int count = 0;
        try {
            String sql = "SELECT COUNT(*)\n"
                    + "  FROM [Delivery]\n"
                    + "  WHERE DeleteFlag = 0";

            try (PreparedStatement ps = connection.prepareStatement(sql);
                 ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            // Handle SQLException
        }
        return 0;
    }

    public void insertDelivery(Delivery deli) {
        try {
            String sql = "INSERT INTO [dbo].[Delivery]\n"
                    + "           ([DeliveryName]\n"
                    + "           ,[Status]\n"
                    + "           ,[DeleteFlag]\n"
                    + "           ,[ShippingFee]\n"
                    + "           ,[ContactNumber])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";

            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setString(1, deli.getDeliveryName());
                ps.setBoolean(2, deli.isStatus());
                ps.setBoolean(3, deli.isDeleteFlag());
                ps.setDouble(4, deli.getShippingFee());
                ps.setString(5, deli.getContactNumber());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            // Handle SQLException
        }
    }

    public void deleteDelivery(int deliveryId) {
        try {
            String sql = "UPDATE [dbo].[Delivery]\n"
                    + "   SET [DeleteFlag] = 1\n"
                    + " WHERE DeliveryId = ?";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setInt(1, deliveryId);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            // Handle SQLException
        }
    }

    public void updateDelivery(Delivery delivery) {
        try {
            String sql = "UPDATE [dbo].[Delivery]\n"
                    + "   SET [DeliveryName] = ?\n"
                    + "      ,[ShippingFee] = ?\n"
                    + "      ,[ContactNumber] = ?\n"
                    + " WHERE DeliveryId = ?";

            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setString(1, delivery.getDeliveryName());
                ps.setDouble(2, delivery.getShippingFee());
                ps.setString(3, delivery.getContactNumber());
                ps.setInt(4, delivery.getDeliveryId());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            // Handle SQLException
        }
    }

    public Delivery getDeliveryById(int id) {
        try {
            String sql = "SELECT * FROM Delivery WHERE DeliveryId = ?";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        return new Delivery(
                                rs.getInt("DeliveryId"),
                                rs.getString("DeliveryName"),
                                rs.getBoolean("Status"),
                                rs.getBoolean("DeleteFlag"),
                                rs.getDouble("ShippingFee"),
                                rs.getString("ContactNumber")
                        );
                    }
                }
            }
        } catch (SQLException e) {
            // Handle SQLException
        }

        return null;
    }
}
