<%@page contentType="text/html" pageEncoding="UTF-8" %>
<head>
    <title>User Profile</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous" />
    <style>
        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }
        .rate:not(:checked) > input {
            position:absolute;
            top:-9999px;
        }
        .rate:not(:checked) > label {
            float:right;
            width:1em;
            overflow:hidden;
            white-space:nowrap;
            cursor:pointer;
            font-size:30px;
            color:#ccc;
        }
        .rate:not(:checked) > label:before {
            content: '★ ';
        }
        .rate > input:checked ~ label {
            color: #ffc700;
        }
        .rate:not(:checked) > label:hover,
        .rate:not(:checked) > label:hover ~ label {
            color: #deb217;
        }
        .rate > input:checked + label:hover,
        .rate > input:checked + label:hover ~ label,
        .rate > input:checked ~ label:hover,
        .rate > input:checked ~ label:hover ~ label,
        .rate > label:hover ~ input:checked ~ label {
            color: #c59b08;
        }
    </style>
</head>
<%@ include file="../../layout/header.jsp" %>
<body>
    <div class="d-flex">
        <%@ include file="../../layout/LeftUserBar.jsp" %>
        <section style="margin-left: 280px; height: calc(92vh); overflow-y: auto;">
            <div class="card my-2 mx-2">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h1 class="fs-2 fw-bold">
                            My Order Details
                        </h1>
                    </div>
                </div>
                <div class="card-body">
                    <div class="p-4">                   
                        <div class="mt-3 container bg-white border rounded-1">  
                            <div class="row">
                                <label for="category-film" class="col-form-label">Full Name: ${requestScope.order.customerName}</label>
                                <label for="category-film" class="col-form-label">Email: ${requestScope.order.customerEmail}</label>
                                <label for="category-film" class="col-form-label">Phone: ${requestScope.order.customerPhone}</label>
                                <label for="category-film" class="col-form-label">Address: ${requestScope.order.customerAddress}</label>
                                <label for="category-film" class="col-form-label">Order date: ${requestScope.order.orderDate}</label>
                            </div>                       
                            <div class="mt-3 container bg-white border rounded-1">   
                                <h3 class="mt-2">Order details</h3>
                                <table class="table mt-3">
                                    <thead>
                                        <tr style="background-color: #00000010;">
                                            <th scope="col">Book Name</th>
                                            <th scope="col">Book Image</th>
                                            <th scope="col">Author Name</th>
                                            <th scope="col">Unit Price</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Total</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="item" items="${order.orderDetails}">
                                            <tr>
                                                <td style="height: 78px;">
                                                    <div class="d-flex align-items-center h-100">
                                                        ${item.book.title}
                                                    </div>
                                                </td>
                                                <td style="height: 78px;">
                                                    <div class="d-flex align-items-center h-100">
                                                        <img src="${item.book.images.get(0).url}" width="50px" height="50px">
                                                    </div>
                                                </td>
                                                <td style="height: 78px;">
                                                    <div class="d-flex align-items-center h-100">
                                                        ${item.book.author.authorName}
                                                    </div>
                                                </td>
                                                <td style="height: 78px;">
                                                    <div class="d-flex align-items-center h-100">
                                                        ${item.book.price}
                                                    </div>
                                                </td>                                       
                                                <td style="height: 78px;">
                                                    <div class="d-flex align-items-center h-100">
                                                        ${item.quantity}
                                                    </div>
                                                </td>
                                                <td style="height: 78px;">
                                                    <div class="d-flex align-items-center h-100">
                                                        ${item.book.price * item.quantity}
                                                    </div>
                                                </td> 
                                                <td style="height: 78px;">
                                                    <div class="d-flex align-items-center h-100 gap-3">
                                                        <c:if test="${!item.isRated && requestScope.order.status == orderFinish}">
                                                            <!-- Button to Open the Modal -->
                                                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal_${item.book.bookId}">
                                                                Rate
                                                            </button>

                                                            <!-- The Modal -->
                                                            <div class="modal" id="myModal_${item.book.bookId}">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">

                                                                        <!-- Modal Header -->
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Order Rating</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                                                        </div>
                                                                        <form action="rate-order" method="post">
                                                                            <!-- Modal body -->
                                                                            <div class="modal-body">
                                                                                <div class="row d-flex">
                                                                                    <input type="hidden" name="bookId" value="${item.book.bookId}">
                                                                                    <input type="hidden" name="orderDetailId" value="${item.orderDetailId}">
                                                                                    <div class="rate">
                                                                                        <input type="radio" id="star5_${item.book.bookId}" name="rate" value="5" />
                                                                                        <label for="star5_${item.book.bookId}" title="text">5 stars</label>
                                                                                        <input type="radio" id="star4_${item.book.bookId}" name="rate" value="4" />
                                                                                        <label for="star4_${item.book.bookId}" title="text">4 stars</label>
                                                                                        <input type="radio" id="star3_${item.book.bookId}" name="rate" value="3" />
                                                                                        <label for="star3_${item.book.bookId}" title="text">3 stars</label>
                                                                                        <input type="radio" id="star2_${item.book.bookId}" name="rate" value="2" />
                                                                                        <label for="star2_${item.book.bookId}" title="text">2 stars</label>
                                                                                        <input type="radio" id="star1_${item.book.bookId}" name="rate" value="1" />
                                                                                        <label for="star1_${item.book.bookId}" title="text">1 star</label>
                                                                                    </div>
                                                                                </div>
                                                                                <label style="font-weight: bold">Comment:</label>
                                                                                <input class="form-control" type="text" name="comment">
                                                                            </div>
                                                                            <!-- Modal footer -->
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-success" data-bs-dismiss="modal">Rate</button>
                                                                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </c:if>
                                                        <c:if test="${!item.isRefund && requestScope.order.status == orderFinish}">
                                                            <!-- Button to Open the Modal -->
                                                            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#refund_${item.book.bookId}">
                                                                Refund
                                                            </button>

                                                            <!-- The Modal -->
                                                            <div class="modal" id="refund_${item.book.bookId}">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">

                                                                        <!-- Modal Header -->
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Order Refund</h4>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                                                        </div>
                                                                        <form action="refund" method="post">
                                                                            <!-- Modal body -->
                                                                            <div class="modal-body">
                                                                                <input type="hidden" name="bookId" value="${item.book.bookId}">
                                                                                <input type="hidden" name="orderDetailId" value="${item.orderDetailId}">

                                                                                <label style="font-weight: bold">Reason</label>
                                                                                <textarea class="form-control" name="reason" rows="4"></textarea>
                                                                                <label style="font-weight: bold">Pick up address</label>
                                                                                <input class="form-control" type="text" name="address">
                                                                            </div>
                                                                            <!-- Modal footer -->
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-success" data-bs-dismiss="modal">Refund</button>
                                                                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                </td> 
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <c:if test="${requestScope.order.status == 1}">
                    <div class="d-flex justify-content-center">
                        <form action="my-order" method="post">
                            <input hidden name="orderId" value="${order.orderId}">
                            <input hidden name="status" id="statusVal" value="6">
                            <button type="submit" class="btn btn-danger mr-2">Cancel</button>
                        </form>
                    </div>
                </c:if>
            </div>
        </section>
    </div>
</body>
<!-- Script using  -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/8d39de38b8.js" crossorigin="anonymous"></script>
