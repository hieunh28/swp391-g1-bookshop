/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Category;
import Model.PrimaryCategory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dell
 */
public class PrimaryCategoryDAO extends DBContext {

    public PrimaryCategory getPCategoryById(int id) {
        try {
            String sql = "Select * From PrimaryCategory Where CategoryId = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                return new PrimaryCategory(rs.getInt("CategoryId"),
                        rs.getString("CategoryName"),
                        rs.getBoolean("Status"),
                        rs.getBoolean("DeleteFlag"));
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public ArrayList<PrimaryCategory> getAllPCategoriesPagnition(int offset, int RecordPerPage) {
        PrimaryCategoryDAO pcDao = new PrimaryCategoryDAO();
        ArrayList<PrimaryCategory> list = new ArrayList<>();
        int count = 0;
        try {
            String sql = "Select * From [PrimaryCategory] Where DeleteFlag = 0\n";
            HashMap<Integer, Object> setter = new HashMap<>();

            sql += "    Order by CategoryId\n";
            sql += "    offset ? rows\n"
                    + " fetch next ? rows only";

            setter.put(++count, offset);
            setter.put(++count, RecordPerPage);

            PreparedStatement ps = connection.prepareStatement(sql);
            for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                PrimaryCategory pc = pcDao.getPCategoryById(rs.getInt("CategoryId"));
                list.add(pc);
            }
        } catch (SQLException e) {

        }
        return list;
    }

    public ArrayList<PrimaryCategory> getAllPCategories() {
        PrimaryCategoryDAO pcDao = new PrimaryCategoryDAO();
        ArrayList<PrimaryCategory> list = new ArrayList<>();
        try {
            String sql = "Select * From [PrimaryCategory] Where DeleteFlag = 0\n";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PrimaryCategory pc = pcDao.getPCategoryById(rs.getInt("CategoryId"));
                list.add(pc);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public int getTotalPCategories() {
        try {
            String sql = "Select count(*) From [PrimaryCategory] Where DeleteFlag = 0\n";
            HashMap<Integer, Object> setter = new HashMap<>();

            PreparedStatement ps = connection.prepareStatement(sql);
            for (Map.Entry<Integer, Object> entry : setter.entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {

        }
        return 0;
    }

    public void insertPCategory(PrimaryCategory pcategory) {
        try {
            String sql = "INSERT INTO [dbo].[PrimaryCategory]\n"
                    + "           ([CategoryName]\n"
                    + "           ,[Status]\n"
                    + "           ,[DeleteFlag])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?)";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, pcategory.getCategoryName());
            ps.setBoolean(2, pcategory.getStatus());
            ps.setBoolean(3, pcategory.getDeleteFlag());
            ps.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public void updateCategory(PrimaryCategory pcategory) {
        try {
            String sql = "UPDATE [dbo].[PrimaryCategory]\n"
                    + "   SET [CategoryName] = ?\n"
                    + " WHERE CategoryId = ?";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, pcategory.getCategoryName());
            ps.setInt(2, pcategory.getCategoryId());
            ps.executeUpdate();
        } catch (SQLException e) {

        }
    }

}
