/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.SaleAdmin;

import DAL.BlogDAO;
import Model.Blog;
import Model.BookImage;
import Model.Constant;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.io.FilenameUtils;

@MultipartConfig
public class ManageBlogController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageBlogController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageBlogController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (request.getParameter("blogId") != null) {
                BlogDAO bDao = new BlogDAO();
                Blog blog = bDao.getBlogById(Integer.parseInt(request.getParameter("blogId")));
                if (blog != null) {
                    request.setAttribute("blog", blog);
                } else {
                    request.getSession().setAttribute("msg", "Book is not exist!");
                }
            }
            request.getRequestDispatcher("/views/SaleAdmin/Blog/create.jsp").forward(request, response);
        } catch (Exception e) {
            request.getSession().setAttribute("msg", "System Error, please try again.");
            response.sendRedirect("admin-books");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        try {
            BlogDAO bDao = new BlogDAO();
            Blog blog;
            switch (request.getParameter("action")) {
                case Constant.Create: {
                    blog = new Blog();
                    blog.setTitle(request.getParameter("title"));
                    blog.setBlogContent(request.getParameter("content"));

                    Date date = Date.valueOf(LocalDate.now());

                    blog.setDatePost(date);
                    blog.setUser(user);
                    blog.setStatus(Constant.StatusActive);
                    blog.setDeleteFlag(Constant.DeleteFalse);

                    //get image send from client
                    List<Part> fileParts = request.getParts().stream()
                            .filter(part -> "file".equals(part.getName()) && part.getSize() > 0 && !part.getSubmittedFileName().isEmpty())
                            .collect(Collectors.toList());

                    //check if there is have file or not
                    if (!fileParts.isEmpty()) {
                        //delete exist image
                        //get path folder to save image
                        String realPath = getServletContext().getRealPath("") + File.separator + "images";
                        for (Part part : fileParts) {
                            //random image name to avoid duplicate name
                            UUID uuid = UUID.randomUUID();
                            String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
                            String fileExtension = FilenameUtils.getExtension(filename);

                            //check if path folder exist or not
                            if (!Files.exists(Paths.get(realPath))) {
                                Files.createDirectory(Paths.get(realPath));
                            }

                            filename = uuid + "." + fileExtension;
                            part.write(realPath + File.separator + filename);

                            String pathImage = "images" + "/" + filename;

                            blog.setBlogImage(pathImage);
                        }
                    }
                    bDao.insertBlog(blog);
                    request.getSession().setAttribute("msg", "Create Succesfully!");
                    break;
                }
                case Constant.Update: {
                    blog = new Blog();
                    blog.setBlogId(Integer.parseInt(request.getParameter("blogId")));
                    blog.setTitle(request.getParameter("title"));
                    blog.setBlogContent(request.getParameter("content"));
                    blog.setBlogImage(request.getParameter("image"));

                    //get image send from client
                    List<Part> fileParts = request.getParts().stream()
                            .filter(part -> "file".equals(part.getName()) && part.getSize() > 0 && !part.getSubmittedFileName().isEmpty())
                            .collect(Collectors.toList());

                    //check if there is have file or not
                    if (!fileParts.isEmpty()) {
                        //delete exist image
                        //get path folder to save image
                        String realPath = getServletContext().getRealPath("") + File.separator + "images";
                        for (Part part : fileParts) {
                            //random image name to avoid duplicate name
                            UUID uuid = UUID.randomUUID();
                            String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
                            String fileExtension = FilenameUtils.getExtension(filename);

                            //check if path folder exist or not
                            if (!Files.exists(Paths.get(realPath))) {
                                Files.createDirectory(Paths.get(realPath));
                            }

                            filename = uuid + "." + fileExtension;
                            part.write(realPath + File.separator + filename);

                            String pathImage = "images" + "/" + filename;

                            blog.setBlogImage(pathImage);
                        }
                    }
                    bDao.update(blog);
                    request.getSession().setAttribute("msg", "Update Succesfully!");
                    break;
                }
                case Constant.Delete: {
                    blog = new Blog();
                    blog.setBlogId(Integer.parseInt(request.getParameter("blogId")));
                    bDao.deleteBlog(blog.getBlogId());
                    
                    request.getSession().setAttribute("msg", "Delete Succesfully!");
                    break;
                }
                default: {
                    request.getSession().setAttribute("msg", "System Error, please try again.");
                }
            }
        } catch (Exception e) {
            request.getSession().setAttribute("msg", e.getMessage());
        }
        response.sendRedirect("sale-admin-list-blog");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
