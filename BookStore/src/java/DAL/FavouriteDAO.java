/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Constant;
import Model.Favourite;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class FavouriteDAO extends DBContext {

    public Boolean likeBook(int userId, int bookId) {
        Favourite fa = getLikeByUserIdAndBookId(userId, bookId);
        if (fa != null) {
            deleteLike(fa.getFavouriteId());
            return false;
        } else {
            insertLike(userId, bookId);
            return true;
        }
    }

    public void deleteLike(int favouriteId) {
        try {
            String sql = "DELETE FROM [Favourites] where FavouriteId = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, favouriteId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FavouriteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public Favourite getFavouriteById(int favouriteId) {
//        try {
//            
//        } catch (SQLException ex) {
//             Logger.getLogger(FavouriteDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
//    
    public ArrayList<Favourite> getListFavouriteByUserId(int userId, int page) {
        ArrayList<Favourite> list = new ArrayList<>();
        try {
            String sql = "Select * From Favourites where UserId = ? Order by FavouriteId OFFSET ? ROW\n"
                    + " FETCH Next ? Rows only";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, (page - 1) * Constant.RecordPerPage);
            ps.setInt(3, Constant.RecordPerPage);
            ResultSet rs = ps.executeQuery();
            Favourite fa = new Favourite();
            BookDAO bDao = new BookDAO();

            while (rs.next()) {
                fa = new Favourite();
                fa.setFavouriteId(rs.getInt("FavouriteId"));
                fa.setBookId(rs.getInt("BookId"));
                fa.setUserId(rs.getInt("UserId"));
                fa.setBook(bDao.getBookById(rs.getInt("BookId")));
                list.add(fa);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FavouriteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void insertLike(int userId, int bookId) {
        try {
            String sql = "INSERT INTO Favourites (BookId, UserId) VALUES (?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bookId);
            ps.setInt(2, userId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FavouriteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Favourite getLikeByUserIdAndBookId(int userId, int bookId) {
        try {
            String sql = "Select * FROM Favourites where UserId= ? and BookId =?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, bookId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Favourite fa = new Favourite();
                BookDAO bDao = new BookDAO();
                UserDAO uDao = new UserDAO();
                fa.setFavouriteId(rs.getInt("FavouriteId"));
                fa.setBookId(rs.getInt("BookId"));
                fa.setUserId(rs.getInt("UserId"));
                fa.setUser(uDao.getUserByID(userId));
                fa.setBook(bDao.getBookById(bookId));
                return fa;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FavouriteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(String[] args) {
        FavouriteDAO faDao = new FavouriteDAO();

        System.out.println(faDao.likeBook(3, 3));
    }
}
