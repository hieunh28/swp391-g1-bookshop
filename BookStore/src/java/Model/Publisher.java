/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Bau
 */
public class Publisher {

    private int publisherId;
    private String publisherName;
    private String country;
    private int foundedYear;
    private boolean status;
    private Boolean deleteFlag;

    /**
     *
     * @return
     */
    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    /**
     *
     * @param deleteFlag
     */
    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @param publisherId
     * @param publisherName
     * @param country
     * @param foundedYear
     * @param status
     * @param deleteFlag
     */
    public Publisher(int publisherId, String publisherName, String country, int foundedYear, boolean status, Boolean deleteFlag) {
        this.publisherId = publisherId;
        this.publisherName = publisherName;
        this.country = country;
        this.foundedYear = foundedYear;
        this.status = status;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @param publisherName
     * @param country
     * @param foundedYear
     * @param deleteFlag
     */
    public Publisher(String publisherName, String country, int foundedYear, Boolean deleteFlag) {
        this.publisherName = publisherName;
        this.country = country;
        this.foundedYear = foundedYear;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    public int getPublisherId() {
        return publisherId;
    }

    /**
     *
     * @param publisherId
     */
    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }

    /**
     *
     * @return
     */
    public String getPublisherName() {
        return publisherName;
    }

    /**
     *
     * @param publisherName
     */
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    /**
     *
     * @return
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     */
    public int getFoundedYear() {
        return foundedYear;
    }

    /**
     *
     * @param foundedYear
     */
    public void setFoundedYear(int foundedYear) {
        this.foundedYear = foundedYear;
    }

    /**
     *
     * Constructor
     */
    public Publisher() {
    }

    /**
     *
     * @return
     */
    public boolean isStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     *
     * @param publisherId
     * @param publisherName
     * @param country
     * @param foundedYear
     * @param deleteFlag
     */
    public Publisher(int publisherId, String publisherName, String country, int foundedYear, Boolean deleteFlag) {
        this.publisherId = publisherId;
        this.publisherName = publisherName;
        this.country = country;
        this.foundedYear = foundedYear;
        this.deleteFlag = deleteFlag;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Publisher{" + "publisherId=" + publisherId + ", publisherName=" + publisherName + ", country=" + country + ", foundedYear=" + foundedYear + ", deleteFlag=" + deleteFlag + '}';
    }

}
