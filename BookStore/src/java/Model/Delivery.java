/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author dell
 */
public class Delivery {
    private int deliveryId;
    private String deliveryName;
    private boolean status;
    private boolean deleteFlag;
    private double shippingFee;
    private String contactNumber; // New field

    public Delivery() {
    }

    public Delivery(int deliveryId, String deliveryName, boolean status, boolean deleteFlag, double shippingFee, String contactNumber) {
        this.deliveryId = deliveryId;
        this.deliveryName = deliveryName;
        this.status = status;
        this.deleteFlag = deleteFlag;
        this.shippingFee = shippingFee;
        this.contactNumber = contactNumber;
    }


    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public boolean isDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public double getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(double shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}

